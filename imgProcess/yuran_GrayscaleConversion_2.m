function yuran_GrayscaleConversion_2( img )
% convert to black and white image using luminosity formula/method.

height = size(img, 1);
width = size(img, 2);

img(:, :, 1) = 0.21 * img(:, :, 1) + 0.72 * img(:, :, 1) + 0.07 * img(:, :, 1);
% compute greyscale value

img(:,:,2:3)=[];
% delete page 2 and 3 to make a 2d-matrix

figure;
imshow(img, []);
title('luminosity method');

imwrite(img, 'yuran_picgray2.jpg');
% save image
end
