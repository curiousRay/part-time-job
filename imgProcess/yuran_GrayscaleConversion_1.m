function yuran_GrayscaleConversion_1( img )
% convert to black and white image using average formula/method.

height = size(img, 1);
width = size(img, 2);

for i = 1:height
    for j = 1:width
        img(i, j, 1) = (img(i, j, 1) + img(i, j, 2) + img(i, j, 3)) / 3;
        % compute average value and save it to page 1.   
    end
end

img(:,:,2:3)=[];
% delete page 2 and 3 to make a 2d-matrix.

figure;
imshow(img, []);
title('average method');

imwrite(img, 'yuran_picgray1.jpg');
% save image
end
