function [x, y, h] = draw_prize()
%draw a single prize and return the location of it

while 1
    x = rand * 10; % 0 <= x <= 10
    y = rand * 10;
    
    if(~((4 <= x) && (x <= 6) && (0 <= y) && (y <= 2)))
        % outside the box
        break;
    end
end

h = plot(x, y, 'o', 'MarkerSize', 15, 'MarkerEdgeColor', 'blue');
hold on;
end
