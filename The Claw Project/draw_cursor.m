function [board_x,board_y, h] = draw_cursor(loc_x, loc_y, joy_speed_x,joy_speed_y, style, size, color)
% Get current position according to last position
% and instant velocity(controlled by joystick)

time_scalar = 0.1;
% controls cursor speed

delta_speed_x = joy_speed_y - 2.5;
delta_speed_y = joy_speed_x - 2.5;

board_x = loc_x + delta_speed_x * time_scalar;
board_y = loc_y + delta_speed_y * time_scalar;

% process boundaries condition
if (board_x < 0)
    board_x = 0;
else
    if (board_x > 10)
        board_x = 10;
    end
end
if (board_y < 0)
    board_y = 0;
else
    if (board_y > 10)
        board_y = 10;
    end
end


h = plot(board_x, board_y, style, 'MarkerSize', size, 'MarkerEdgeColor', color);

end

