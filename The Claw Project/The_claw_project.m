clear all;clc;close all;
%% configure arduino
a = ArduinoSimJoystick;
a.configurePin('D5','pullup');
button = a.readDigitalPin('D5');

%% vary difficulties
prize_total = 5;
time_limit = 90; %default is 30
detect_radius = 0.5;

%% define variables and configure plot
score = 0;
prize_loc = zeros(prize_total, 2);
prize_collected = 0;
prize_index_attached = 0;
is_grabbing = 0;
is_dropping = 0;
reset_cursor = 0;
last_x_cursor = 5;
last_y_cursor = 1;

axis([0 10 0 10]);grid on;
title('The Claw Game');
rectangle('Position',[4,0,2,2],'LineWidth',2,'EdgeColor','black');
hold on;
for i=1:prize_total
    [loc_x, loc_y, ~] = draw_prize();
    prize_loc(i, 1) = loc_x;
    prize_loc(i, 2) = loc_y;
end

tic
while toc <= time_limit
    % obtain real-time joystick input
    % right click the button to set it to 0 
    speed_x = a.readVoltage('A0');
    speed_y = a.readVoltage('A2');
    button = a.readDigitalPin('D5');
    if (~button)
        if (~is_grabbing)
            is_grabbing = 1;
        else
            is_dropping = 1;
        end
    end
%     fprintf('(x,y) = (%8g,%8g), btn = %g\n',speed_x,speed_y,button)
    
    if (~is_grabbing)
        % remove trace of cursor
        h = findobj(gca, 'Type','line', '-and', 'MarkerEdgeColor', 'red');
        if length(h) == 1
        	delete(h(1))
        end
        [x, y, h] = draw_cursor(last_x_cursor, last_y_cursor, speed_x, speed_y, 'x', 10, 'red');  
    else
        % delete the remaining red cursor
        delete(findobj(gca, 'Type','line', '-and', 'MarkerEdgeColor', 'red'));
        
        % remove trace of new cursor
        h = findobj(gca, 'Type','line', '-and', 'MarkerEdgeColor', 'green', 'MarkerSize', 15);
        if length(h) == 1
        	delete(h(1))
        end
        [x, y, h] = draw_cursor(last_x_cursor, last_y_cursor, speed_x, speed_y, '*', 15, 'green');
        
        % hide origin blue prize
        h_origin_prize = findobj(gca, 'Type', 'line', '-and', 'XData', prize_loc(prize_index_attached, 1), '-and', 'YData', prize_loc(prize_index_attached, 2));
        h_origin_prize.Visible = 'off';
        
        % draw a moving prize
        h = findobj(gca, 'Type','line', '-and', 'MarkerEdgeColor', 'green', '-and', 'Marker', '*', '-and', 'MarkerSize', 16);
        if length(h) == 1
        	delete(h(1))
        end
        % change prize circle to this
        [x1, y1, h] = draw_cursor(last_x_prize, last_y_prize, speed_x, speed_y, '*', 16, 'green');
        last_x_prize = x1;
        last_y_prize = y1;
        
        if (is_dropping)
            is_grabbing = 0;
            is_dropping = 0;
            delete(findobj(gca, 'Type','line', '-and', 'MarkerEdgeColor', 'green', '-and', 'Marker', '*', '-and', 'MarkerSize', 15));
            % judge if the prize is in the box
            if(x1>=4 && x1<=6 && y1>=0 && y1<=2)
                % success
                score = score + 10;
                prize_collected = prize_collected + 1;
                
            else
                % fail
                h.MarkerEdgeColor = 'blue';
                h.Marker = 'o';
                prize_loc(prize_index_attached,1) = last_x_prize;
                prize_loc(prize_index_attached,2) = last_y_prize;
                reset_cursor = 1;
            end
            delete(findobj(gca, 'Type','line', '-and', 'MarkerEdgeColor', 'green', '-and', 'MarkerSize', 15));
        end
        
    end
        
    % detect prizes
    for i=1:prize_total
        dist = calc_dist(x, y, prize_loc(i,1), prize_loc(i, 2));
        if (dist <= detect_radius)
%             disp('Please press the switch and grab the prize!');
            prize_index_attached = i;
            last_x_prize = prize_loc(prize_index_attached,1);
            last_y_prize = prize_loc(prize_index_attached,2);
            break;
        end
    end
    
    % grab attached prizes
    
    if (~reset_cursor)
        last_x_cursor = x;
        last_y_cursor = y;
    else
        last_x_cursor = 5;
        last_y_cursor = 1;
        reset_cursor = 0;
    end
    % update info
    xlabel(['Time elapsed: ', num2str(toc), '  ', 'Score: ', num2str(score)]);
    ylabel(['Targets left: ', num2str(prize_total - prize_collected)]);
    pause(0.1)
end

disp(['Game ended, your score is: ', num2str(score), '.']);


