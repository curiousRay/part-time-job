classdef ArduinoSimJoystick < handle
	
   properties 
       Port = 'COMVirtual';
       Board = 'Uno';
       AvailablePins = {'D2-D13', 'A0-A5'};
       AvailableDigitalPins = {'D2-D13', 'A0-A5'};
       AvailablePWMPins = {'D3', 'D5-D6', 'D9-D11'};
       AvailableAnalogPins = {'A0-A5'};
       AvailableI2CBusIDs = [0];
       Libraries = {'I2C', 'SPI', 'Servo'};
   end
	
	properties (Hidden)
		JoyStickButtonPin='d5';
		JoyStickXAxisPin='a2';
		JoyStickYAxisPin='a0';
		
		LeftDown=0;
		RightDown=0;
		Joystick;
		FigureHandle;
		AxesHandle;
		isPullUp=0;
	end
	
	methods 
        function value = readDigitalPin(self,varargin)
            %Input check
            if length(varargin) < 1
                error('Not enough input arguments.');
            elseif length(varargin) > 1
                error('Too many input arguments.');
            elseif ~ishandle(self.FigureHandle)
                error('Error writing IOServerBlock. \nInvalid operation. Object must be connected to the serial port.',0);
            end
            
            %Read the button
            pin = varargin{1};
            switch lower(pin)
                case self.JoyStickButtonPin
                    if self.isPullUp
                        value = double(~self.RightDown);
                    else
                        value = 0;
                    end
                otherwise
                    value = 0;
			end
			pause(8.7e-3+1.3e-3*rand(1));
        end
        
        function configurePin(self,varargin)
            %Input check
            if length(varargin) < 2
                error('Not enough input arguments.');
            elseif length(varargin) > 2
                error('Too many input arguments.');
            elseif ~ishandle(self.FigureHandle)
                error('Error writing IOServerBlock. \nInvalid operation. Object must be connected to the serial port.',0);
            end
            
            %set the LEDs
            pin = varargin{1};
            value = varargin{2};
            switch lower(pin)
                case self.JoyStickButtonPin
                    switch lower(value)
                        case "pullup"
                            self.isPullUp = 1;
                        otherwise
                            error('Invalid digital pin mode. The mode must be a character vector or string. Valid mode values are:\nTone, DigitalInput, DigitalOutput, Pullup, PWM, Servo, SPI, Interrupt, Ultrasonic, Unset.',0)
                    end
                otherwise
                
			end
			pause(8.7e-3+1.3e-3*rand(1));
		end
		
		function volts=readVoltage(self,varargin)
            %Input check
            if length(varargin) < 1
                error('Not enough input arguments.');
            elseif length(varargin) > 1
                error('Too many input arguments.');
            elseif ~ishandle(self.FigureHandle)
                error('Error writing IOServerBlock. \nInvalid operation. Object must be connected to the serial port.',0);
			end
			
            %Read the pin
            pin = varargin{1};
            switch lower(pin)
                case self.JoyStickXAxisPin
					volts = self.Joystick.XData*2.5+2.5;
				case self.JoyStickYAxisPin
					volts = self.Joystick.YData*2.5+2.5;
                otherwise
                    volts = 0;
			end  
			volts=volts+0.5e-3*rand(1);
			pause(8.7e-3+1.3e-3*rand(1));
		end
		
	end
	
	methods (Hidden)
		function self=ArduinoSimJoystick(varargin)
			pause(3); %match up the time of real arduino
			self.FigureHandle=figure('MenuBar','none','ToolBar','none','HandleVisibility','Callback','Name','Arduino','NumberTitle','off');
			self.FigureHandle.Position(3:4)=[210 210];
			self.AxesHandle=axes(self.FigureHandle);
			x=-1:0.01:1;
			y=sqrt(1-x.^2);
			plot(self.AxesHandle,x,y,'b',x,-y,'b')
			self.AxesHandle.NextPlot='add';
			self.Joystick=plot(self.AxesHandle,0,0,'o','MarkerSize',10,'LineWidth',10);
			xlim(self.AxesHandle,[-1 1])
			ylim(self.AxesHandle,[-1 1])
			self.AxesHandle.XTick=[];
			self.AxesHandle.YTick=[];
			self.Joystick.ButtonDownFcn=@self.MouseDownOnTarget;
			self.FigureHandle.WindowButtonDownFcn=@self.MouseDown;
			self.FigureHandle.WindowButtonUpFcn=@self.MouseUp;
			drawnow;
		end
		
		function MouseDown(self,src,evt)
			if strcmp(self.FigureHandle.SelectionType,'alt')
				%fprintf('Right Down\n');
				self.AxesHandle.Color=[0.8 0.8 0.8];
				self.RightDown=1;
			end
		end
		
		%The buttondown(not window callback) provides the button! yay!
		function MouseDownOnTarget(self,src,evt)
			if evt.Button==1 %left click
				%fprintf('Left Down on Target\n');
				self.LeftDown=1;
				self.FigureHandle.WindowButtonMotionFcn=@self.MouseMove;
			elseif evt.Button==3 %right click
				%fprintf('Right Down on Target\n');
				self.RightDown=1;
			end
		end
		function MouseUp(self,src,evt)
			if strcmp(self.FigureHandle.SelectionType,'normal') || (self.LeftDown && ~self.RightDown)
				%fprintf('Left Up\n');
				self.LeftDown=0;
				self.Joystick.XData=0;
				self.Joystick.YData=0;
				self.FigureHandle.WindowButtonMotionFcn=[];
			elseif strcmp(self.FigureHandle.SelectionType,'alt')
				%fprintf('Right Up\n');
				self.RightDown=0;
				self.AxesHandle.Color=[1 1 1];
			end
		end
		function MouseMove(self,src,evt)
			%fprintf('Move\n');
			if self.LeftDown	
				% convert from pixels to units
				self.AxesHandle.Units='pixels';
				NewXPos=(src.CurrentPoint(1)-self.AxesHandle.InnerPosition(1))*(self.AxesHandle.XLim(2)-self.AxesHandle.XLim(1))/self.AxesHandle.InnerPosition(3)+self.AxesHandle.XLim(1);
				NewYPos=(src.CurrentPoint(2)-self.AxesHandle.InnerPosition(2))*(self.AxesHandle.YLim(2)-self.AxesHandle.YLim(1))/self.AxesHandle.InnerPosition(4)+self.AxesHandle.YLim(1);
				if NewXPos^2+NewYPos^2<1 %inside unit circle
					self.Joystick.XData=NewXPos;
					self.Joystick.YData=NewYPos;
				else %outside unit circle
					Short=NewXPos+1i*NewYPos; %convert to complex and normalize
					Short=Short/abs(Short);
					self.Joystick.XData=real(Short);
					self.Joystick.YData=imag(Short);
				end		
				self.AxesHandle.Units='normalized';
			end
		end
	end
	
	
end