import numpy as np
import matplotlib.pyplot as plt


def chunks(list, n):
    return [list[i:i + n] for i in range(0, len(list), n)]


def signaldenoiser(sig, num):
    slices = chunks(sig, int(len(sig) / num))
    for slice in slices:
        slice.fill(slice.mean())
    slices = np.array(slices)
    return slices.flatten()


# Provided variables
num_samples = 250
num_subintervals = 10
signal = np.array([1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                   1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                   1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 6,
                   6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
                   6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
                   6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
                   6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
                   6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
                   6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 1, 1, 1,
                   1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                   1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                   1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 10, 10, 10, 10,
                   10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10,
                   10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10,
                   10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 3, 3, 3, 3, 3,
                   3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
                   3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
                   3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
                   3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
                   3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
                   3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 5, 5, 5, 5, 5, 5, 5,
                   5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5,
                   5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5,
                   5, 5, 5, 5, 5, 5, 5, 5, 5, 4, 4, 4, 4, 4, 4, 4, 4,
                   4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
                   4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
                   4, 4, 4, 4, 4, 4, 4, 4, 9, 9, 9, 9, 9, 9, 9, 9, 9,
                   9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
                   9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
                   9, 9, 9, 9, 9, 9, 9])

signal_resampled = signal[0::2]
sampling_locations = np.arange(0, 10, 10 / num_samples)  # create an array with the sample locations in [0,1]

mu = 0  # mean
sigma = 3  # standard deviation
noise = np.random.normal(mu, sigma, num_samples)
signal_new = signal_resampled + noise

denoise = signaldenoiser(signal_new, num_subintervals)

plt.subplot(221)
plt.title('Noisy signal with 1/2th the samples')
plt.xlabel('Sampling location')
plt.ylabel('Signal')
plt.plot(sampling_locations, noise)

plt.subplot(223)
plt.title('Denoised signal with 1/2th the samples')
plt.xlabel('Sampling location')
plt.ylabel('Signal')
plt.plot(sampling_locations, signal_resampled, label='Origin signal')
plt.plot(sampling_locations, denoise, label='Denoised signal')
plt.legend(loc='upper right')

###################
num_samples = 50
signal_resampled = signal[0::10]
sampling_locations = np.arange(0, 10, 10 / num_samples)  # create an array with the sample locations in [0,1]
noise = np.random.normal(mu, sigma, num_samples)
signal_new = signal_resampled + noise
denoise = signaldenoiser(signal_new, num_subintervals)

plt.subplot(222)
plt.title('Noisy signal with 1/10th the samples')
plt.xlabel('Sampling location')
plt.ylabel('Signal')
sampling_locations = np.arange(0, 10, 10 / num_samples)  # create an array with the sample locations in [0,1]
plt.plot(sampling_locations, noise)

plt.subplot(224)
plt.title('Denoised signal with 1/10th the samples')
plt.xlabel('Sampling location')
plt.ylabel('Signal')
plt.plot(sampling_locations, signal_resampled, label='Origin signal')
plt.plot(sampling_locations, denoise, label='Denoised signal')
plt.legend(loc='upper right')

###################

plt.tight_layout()
plt.show()




