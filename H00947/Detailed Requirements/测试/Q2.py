import numpy as np
import matplotlib.pyplot as plt

# x1 = np.random.uniform(0, 1, 100)
# x2 = np.random.uniform(0, 1, 1000)
# x3 = np.random.uniform(0, 1, 100000)
# s = 20
ns = [10, 100, 500, 1000, 5000, 10000, 50000, 100000, 500000, 1000000, 5000000]
averages = np.zeros(len(ns))


def compute_average(x):
    total = 0
    for i in x:
        total = total + i
    return total / len(x)


def partition_array(x, s):
    thresholds = np.arange(0, 1, 1 / s)
    mid_points = np.zeros(s)
    numentries_subintervals = np.zeros(s)
    res = [[] * s for _ in range(s)]
    for inx, threshold in enumerate(thresholds):
        for num in x:
            if threshold <= num < (threshold + 1 / s):
                res[inx].append(num)
        mid_points[inx] = np.median(res[inx])
        mid_points = mid_points.round(1)
        numentries_subintervals[inx] = len(res[inx])
        numentries_subintervals = numentries_subintervals.astype(np.int)
    return numentries_subintervals, mid_points


#
# result1 = partition_array(x1, s)
# result2 = partition_array(x2, s)
# result3 = partition_array(x3, s)
#
# #print("The number of entries in the subintervals are %s" % (result[0]))
# #print("The subinterval mid points of the intervals are %s" % (result[1]))
#
# plt.subplot(221)
# plt.bar(result1[1], result1[0], width=1/s, label='s=20, n=100', lw=3)
# plt.legend(loc='upper left')
#
# plt.subplot(222)
# plt.bar(result2[1], result2[0], width=1/s, label='s=20, n=1000', lw=3)
# plt.legend(loc='upper left')
#
# plt.subplot(223)
# plt.bar(result3[1], result3[0], width=1/s, label='s=20, n=100000', lw=3)
# plt.legend(loc='upper left')
#
# plt.tight_layout()
# plt.show()

for inx, n in enumerate(ns):
    x = np.random.uniform(0, 1, n)
    averages[inx] = compute_average(x)

print(averages)
plt.title('Relationship between the Average and Length in Uniform Distribution')
plt.xscale('log')
plt.xlabel('Length')
plt.ylabel('Average')
plt.plot(ns, averages)
plt.show()