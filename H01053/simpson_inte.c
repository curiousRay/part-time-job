/******************************************************************************

                            Online C Compiler.
                Code, Compile, Run and Debug C program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/

#include <stdio.h>
#include <math.h>
#define PI 3.14

double beijihanshu(double x) {
    return sin(x);
}

double int_simpson(double from, double to, double n)
{
   double h = (to - from) / n;
   double sum1 = 0.0;
   double sum2 = 0.0;
   int i;
   for(i = 1;i < n;i++) {
      sum1 += beijihanshu(from + h * i + h / 2.0);
      sum2 += beijihanshu(from + h * i);
   }

   sum1 = sum1 + beijihanshu(from + h / 2.0);
   return h / 6.0 * (beijihanshu(from) + beijihanshu(to) + 4.0 * sum1 + 2.0 * sum2);
}



int main()
{
    double res = int_simpson(0, PI, 100);
    printf("%lf", res);

    return 0;
}


