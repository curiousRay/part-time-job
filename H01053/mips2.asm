.data
	# step:    .double 20.0
	input_start: .asciiz "plz input start: "
	input_end: .asciiz "plz input end: "
	input_n: .asciiz "plz input n: "

	float_1:.float 5.0
	float_2:.float 21.0
	double_3: .double 0.0
	double_4: .double 0.0
	float_5 :.float 2.0
	float_6: .float 4.0
.text
	lwc1    $f20, float_1
	lwc1	$f21, float_2
	ldc1	$f24, double_3 #sum1
	ldc1    $f26, double_4 #sum2
	lwc1	$f22, float_5
	lwc1	$f23, float_6
	
	#ldc1	$f4, step
.globl main

main:
	la $a0, input_start #show msg
	li $v0, 4
	syscall
	li $v0, 7  # read_double
	#li $v0, 3  # print_double 打印所输入的参数
	syscall
	mov.d	$f4, $f0 #将输入的参数固定地储存到f4,为下限
	
	la $a0, input_end #show msg
	li $v0, 4
	syscall
	li $v0, 7  # read_double
	syscall
	mov.d	$f6, $f0 #将输入的参数固定地储存到f6,为上限
		
	la $a0, input_n #show msg
	li $v0, 4
	syscall
	li $v0, 7  # read_double
	syscall
	mov.d	$f8, $f0 #将输入的参数固定地储存到f8,为n值
	
	#add.d	$f8,$f8,$f6
	#add.d	$f8,$f8,$f4
	#mov.d	$f12,$f8 # test print:必须先把数值移动到f12才能输出(此输出为三个参数之和)
	#li $v0, 3
	#syscall
	j simpson_inte
exit:
	li $v0,10
	syscall
	
simpson_inte:
	li $t0, 1 #for循环index的初始值
    	cvt.w.d $f3, $f8
    	mfc1 $t1,$f3	# convert double n to int n(store in t1)
    	mov.d $f10, $f8 # backup value of n to f10
    	sub.d $f8, $f6, $f4
    	div.d $f8, $f8, $f10 # f8 is double h, now the f10 canbe wasted
loop:
    	bge $t0, $t1, exit # branch if($t0 >= t1) 
    	# loop body
    	mtc1.d $t0, $f10
  	cvt.d.w $f10, $f10 # convert int t0(aka. i) to double, store it into f10, n -> i
  	### convert 
    	mul.d $f12, $f8, $f10 # h * i
    	add.d $f14, $f12, $f4 # from + h * i
    	mov.d $f10, $f8
    	cvt.d.s $f30, $f22 # load const 2.0 to f30
    	div.d $f10, $f10, $f30 # h / 2.0
    	add.d $f12, $f12, $f4 # from + h * i, param
    	jal beijihanshu
	#li $v0, 3
	mov.d $f12,$f2
	add.d $f26, $f26, $f12 # sum2 +=...
    	add.d $f14,$f14, $f10 # from + h * i + h / 2.0
    	mov.d $f12, $f14
    	jal beijihanshu
	#li $v0, 3
	mov.d $f12,$f2
	add.d $f24, $f24, $f12 # sum1 +=...
	addi    $t0, $t0, 1 # $t0++ for loop increment
    	j   loop # for loop ends here
    	
	add.d $f10, $f10, $f4 # from + h / 2.0
	mov.d $f12, $f10
	jal beijihanshu
	#li $v0, 3
	mov.d $f12,$f2
	add.d $f24, $f24, $f12 # sum1 = sum1 + beijingahnshu (from + h / 2,0)
	
	cvt.d.s $f30, $f23
	mul.d $f24, $f24,$f30 # 4.0*sum1 
	cvt.d.s $f30, $f22
	mul.d $f26, $f26,$f30 # 2.0*sum2
	add.d $f24, $f24, $f26 # 4.0*sum1 + 2.0*sum2
	mov.d $f12, $f4
	jal beijihanshu
	#li $v0, 3
	mov.d $f12,$f2
	add.d $f24,$f24,$f12 # 4.0*sum1 + 2.0*sum2 + beijihanshu(from)
	mov.d $f12, $f6
	jal beijihanshu
	#li $v0, 3
	mov.d $f12,$f2
	add.d $f24,$f24,$f12 # 4.0*sum1 + 2.0*sum2 + beijihanshu(from) + beijihanshu(to)
	# 此时f30为2.0
	mul.d $f24,$f24,$f8 #以上表达式乘以h
	add.d $f30,$f30,$f30
	add.d $f30,$f30,$f30 #构造6.0
	div.d $f24, $f24, $f30 # 得到最终返回值
	li $v0, 3
	mov.d $f12,$f24 #打印最终结果
	syscall
	j exit

beijihanshu:
	cvt.d.s $f30, $f20 # 5.0 -> f30

	mov.d $f16, $f12 # copy param
	mul.d $f12, $f12, $f12 # x*x
	mul.d $f12, $f12, $f30 # 5*x*x
	
	cvt.d.s $f30, $f21 # 21.0 -> f30
	
	div.d $f30, $f30, $f16 # 21/x
	div.d $f30, $f30, $f16 # 21/x/x
	div.d $f30, $f30, $f16 # 21/x/x/x
	div.d $f30, $f30, $f16 # 21/x/x/x/x
	add.d $f2,$f12,$f30 # final expression
	jr $ra
