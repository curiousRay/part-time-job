function [h,x]=Zhang_trajectory(v,A)
g=9.81;
A=A/180*pi;
t_hit=2*v*sin(A)/g;
t=linspace(0,t_hit,100);
h=v*t*sin(A)-1/2*g*t.^2;
x=v*t*cos(A);
end