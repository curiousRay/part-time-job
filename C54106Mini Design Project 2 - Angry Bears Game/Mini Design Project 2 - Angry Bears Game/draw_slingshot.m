function [shotX,shotY] = draw_slingshot(x,y,A)
% DRAW_SLINGSHOT Plots a slingshot with given angle.
%   DRAW_SLINGSHOT(X,Y,A) plots a slingshot with base at (X,Y) and the
%   sling straps at angle A.
%
%   Notes:
%    - A is in degrees from positive x-axis.
%    - The dimensions of the resulting images are set inside the function.
%
%   [SHOTX,SHOTY] = DRAW_SLINGSHOT(X,Y,A) also returns the x,y position of
%   the middle of the projectile.
%
%   John Miller, 2019-04-10


% Define colors
tan = [210,180,140]/256;
brown = [139,69,19]/256;
dark_brown = brown*0.7;

% Set dimensions (all in "figure units" which shrink or grow as the figure
% is scaled up and down) 
shaft_height = 4;
shaft_thick = 6;            % line thickness
fork_height = 6;
fork_width = 5;
fork_thick = shaft_thick;   % line thickness
band_length = 8;
band_thick = 4;            % line thickness
bear_size = 0.02;

% Get previous hold status
prev_hold = ishold;
hold on

% -- Main (bottom) shaft -- 
% A straight line
shaft_x = [x x];
shaft_y = [y y+shaft_height];
line(shaft_x,shaft_y,'LineWidth',shaft_thick,'Color',tan)

% -- Right side of fork --
% This is a parabola with a slightly steeper slope than the left side (to
% give a 3D effect). 
% Parabola scaling factor (larger = steeper)
Rfork_s = 6;                 
% Temporary x values for calculating parabola. Ending point is calculated
% to result in a parabola that ends at the desired height.
temp = linspace(0,sqrt(fork_height/Rfork_s),101);
% Final x values are shifted by base x location
Rfork_x = x + temp;
% Final y values are the parabola shifted by base y and the shaft height
Rfork_y = y + shaft_height + Rfork_s*(temp.^2);
line(Rfork_x,Rfork_y,'LineWidth',fork_thick,'Color',tan)

% -- Right side band --
% Draw as a straight line from near the top of the right fork
Rband_x = [ Rfork_x(end-5)  Rfork_x(end-5)-band_length*cosd(A) ];
Rband_y = [ Rfork_y(end-5)  Rfork_y(end-5)-band_length*sind(A) ];
line(Rband_x,Rband_y,'LineWidth',band_thick,'Color',brown)

% -- Left side of fork --
% Parabola scaling factor (larger = steeper)
Lfork_s = 4;                 
% Temporary x values for calculating parabola. Ending point is calculated
% to result in a parabola that ends at the desired height.
temp = linspace(0,sqrt(fork_height/Lfork_s),101);
% Final x values are flipped (to the left) and shifted by base x location
Lfork_x = x - temp;
% Final y values are the parabola shifted by base y and the shaft height
Lfork_y = y + shaft_height + Lfork_s*(temp.^2);
line(Lfork_x,Lfork_y,'LineWidth',fork_thick,'Color',tan)

% -- Bear --
% Load bear from icon function. Scale down.
[bear_x,bear_y] = bear_icon(bear_size);
% Shift from origin (0,0) to end of the band
bear_x = bear_x + Rband_x(end);
bear_y = bear_y + Rband_y(end);
% Draw
fill(bear_x,bear_y,dark_brown)

% -- Left side band --
% Draw as a straight line from near the top of the left fork to the end
% point of the right band (rather than trying to calculate, this ensures
% they match up exactly)
Lband_x = [ Lfork_x(end-5)  Rband_x(2) ];
Lband_y = [ Lfork_y(end-5)  Rband_y(2) ];
line(Lband_x,Lband_y,'LineWidth',band_thick,'Color',brown)

% --- Assign output ---
% Find middle of bear
shotX = (min(bear_x)+max(bear_x))/2;
shotY = (min(bear_y)+max(bear_y))/2;

% Return to previous hold status
if ~prev_hold
    hold off
end

end
