function [score] = hit_test(proj_loc, tar_loc)
%Determines the score a player receives for a given shot.

if proj_loc >= tar_loc(1) && proj_loc <= tar_loc(3)
    score = 50;
else
    score = 0;
end

