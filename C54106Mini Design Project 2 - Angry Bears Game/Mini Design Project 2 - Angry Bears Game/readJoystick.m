function [mag, ang] = readJoystick(mag,ang,a,pinX,pinY,pinBtn)
% READJOYSTICK Angry Bears helper function for reading joystick. 
% [MAG, ANG] = READJOYSTICK(MAG,ANG,A,PINX,PINY,PINBTN) returns the
% magnitude MAG and angle ANG values adjusted by the Player using the
% joystick, connected to Arduino A on pins PINX, PINY, and PINBTN.
%
% While getting input from the user, feedback is given by printing the
% values of MAG and ANG as an x-axis label on the current axes.
%
% EGR 1302, John Miller, 2019-04-09

% Check inputs
if ~isnumeric(mag) || mag < 0
    error('Input MAG must be a positive, numeric variable.')
end
if ~isnumeric(ang) || ang < 0
    error('Input ANG must be a positive, numeric variable.')
end
if ~isa(a,'arduino')
    error('Input A must be an Arduino object.')
end
if ~any(strcmp(pinX,a.AvailablePins))
    fprintf('Pins available on Arduino A:\n')
    disp(a.AvailablePins)
    error('Input PINX must be one of the available pins on Arduino A.')
end
if ~any(strcmp(pinY,a.AvailablePins))
    fprintf('Pins available on Arduino A:\n')
    disp(a.AvailablePins)
    error('Input PINY must be one of the available pins on Arduino A.')
end
if ~any(strcmp(pinBtn,a.AvailablePins))
    fprintf('Pins available on Arduino A:\n')
    disp(a.AvailablePins)
    error('Input PINBTN must be one of the available pins on Arduino A.')
end

% Main loop
while a.readDigitalPin(pinBtn)
    
    % Get joystick position
    x = a.readVoltage(pinX);
    y = a.readVoltage(pinY);

    % Ignore dead zone in center
    if x < 2.4 || x > 2.6
        % Adjust proportionally
        % Note: negative because joystick x-axis is 5 to 0 (L to R)
        % when y-axis is correct (0 to 5, bottom to top)
        mag = mag - (x-2.5);
    end
    % Ignore dead zone in center
    if y < 2.4 || y > 2.6
        % Adjust proportionally
        ang = ang + (y-2.5);
    end
    
    % Limit values
    if mag < 0, mag = 0; end
    if mag > 100, mag = 100; end
    if ang < 0, ang = 0; end
    if ang > 180, ang = 180; end
    
    % Print position for user
    xlabel(sprintf('Mag = %.1f, Angle = %.1f^o',mag,ang))
    
    pause(0.1)
end
