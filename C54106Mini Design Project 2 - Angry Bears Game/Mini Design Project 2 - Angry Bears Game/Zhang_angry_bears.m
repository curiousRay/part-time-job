clear all;clc;close all;
%% configure arduino
MyArduino = arduino('COM3', 'Uno');
pin_btn = 'D5';
pin_x = 'A0';
pin_y = 'A2';
MyArduino.configurePin(pin_btn,'pullup');

%% define variables
score = 0;
shots_left = 5;
target_x = 20 + rand * 80; % 20 <= x <= 100
target_size = 1;
target_color = 'r';
mag = 10;
ang = 30;
sling_x = 10;
sling_y = 0;
axis([-100 100 0 50]);grid on;
draw_slingshot(sling_x, sling_y, ang);
hold on;
tar_loc = draw_target(target_x, 0, target_size, target_color);
hold on;

%% start shooting loop
while shots_left > 0
    while MyArduino.readDigitalPin('D8')
        [mag_new, ang_new] = readJoystick(mag, ang, MyArduino, pin_x, pin_y, pin_btn);
        mag = mag_new;
        ang = ang_new;
    end
    % mag and ang are tweaked values
    [h, x] = Zhang_trajectory(mag,ang);
    plot(sling_x + x, sling_y + h); % the start point of trajectory line is based on slingshot position
    hold on;
    shots_left = shots_left - 1;
    this_score = hit_test(x(end), tar_loc);
    score = score + this_score;

    pause(0.5);
    
end

%% display result
disp(['Your final score is: ', num2str(score)]);