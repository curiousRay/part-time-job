function [bx,by] = draw_target(x,y,s,c)
% DRAW_target Plots a small target.
%   DRAW_TARGET(X,Y,S,C) plots a target located at (X,Y) with size S and
%   color C. S is a scaling factor. S > 1 makes the target larger. The 
%   bottom-center (middle) of the target is located at (X,Y).
% 
%   [BX,BY] = DRAW_TARGET(...) also returns the x- and y-coordinates of the
%   vetices of the target body (in the respective vectors BX and BY),
%   starting in the bottom-left, as shown here:
%               (2) .-------. (3)
%                   |       |
%               (1) .-------. (4)
%
%   Notes:
%    - C is a color specifier (e.g. 'r' for red) or RGB vector.
%    - The default dimensions of the target are set inside the function.
%
%   John Miller, 3/24/2018

% Default dimensions & scaling
h = 5*s;      % Target height
w = 10*s;     % Target width

% Get previous hold status
prev_hold = ishold;
hold on

% Body
bx = [x-w/2 x-w/2 x+w/2 x+w/2];
by = [y y+h y+h y];
fill(bx,by,c)

% Return to previous hold status
if ~prev_hold
    hold off
end

% Note: Outputs (bx & by) are assigned above.

