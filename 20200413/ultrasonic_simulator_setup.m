%% Arduino Lab 3 - Ultrasonic Sensor Test
% EGR 1302, Skurla, 4/6/20

% Setup arduino - ONLY RUN ONCE (when you connect Arduino)
clear;clc;close all;
MyArduino=ArduinoSimUltrasonic
sensor = MyArduino.ultrasonic('D4','D6')  % trig pin, echo pin
MyArduino.configurePin('D8','pullup')     % button

%% Read Distance from Sensor

sampling_rate = 5; % sampling times per sec
MAX_SPACE = 500; % preallocation memory
window_size = 100;% number of data points to show at one time

t = 0:1:MAX_SPACE - 1;
s = zeros(1, MAX_SPACE);
ax = subplot(1,1,1);
dtick = 1/sampling_rate;
tick_offset = (0:dtick:window_size);
update_tick = 0;

for t2 = 1:1:MAX_SPACE
  if MyArduino.readDigitalPin('D8')
    val = readDistance(sensor);
    if (t2>1 && s(t2-1)>0.5 && val == inf) % wash inf value to be 0 or 1
        s(t2) = 1;
    else
        if (t2>1 && s(t2-1)<0.5 && val == inf)
            s(t2) = 0;
        else
            if (t2 == 1)
                s(t2) = 0;
            else
                s(t2) = val;
            end
        end
    end

    plot(t, s);grid on;
    xlabel(['Ticks: ',mat2str(dtick), ' sec(s)/tick']);ylabel('Distance');
    set(ax,'ylim',[0 1])
    if t2 < window_size
        set(ax,'xlim',[0 window_size]) % do not move the data window
    else
        set(ax,'xlim',[t2-window_size t2]) % move the data window
    end
    
    update_tick = update_tick + 1;
    pause(dtick);
    drawnow;
  end

end
