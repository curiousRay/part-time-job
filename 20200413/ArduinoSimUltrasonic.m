classdef ArduinoSimUltrasonic < handle
    %Arduino Simulator for Ultrasonic sensor
    
    % To-do:
    % Improve graphics.
   properties 
       Port = 'COMVirtual';
       Board = 'Uno';
       AvailablePins = {'D2-D13', 'A0-A5'};
       AvailableDigitalPins = {'D2-D13', 'A0-A5'};
       AvailablePWMPins = {'D3', 'D5-D6', 'D9-D11'};
       AvailableAnalogPins = {'A0-A5'};
       AvailableI2CBusIDs = [0];
       Libraries = {'I2C', 'SPI', 'Servo', 'Ultrasonic'};
   end
   
   properties (Hidden)
       hFigure=[]; %handles
       hAxes=[];
       hButton=[];
       isPullUp=0;
       hLED1=[];
       hLED2=[];
       Slider=struct('h',[],'X',[],'Y',[],'PixelRatio',[],'Value',[]);  % set in constructor
       Hand=struct('h',[],'Width',[],'Height',[]);  % set in constructor
   end
   
   properties (Hidden, Constant)
       DIGITAL_PINS = {'D2','D3','D4','D5','D6',...
                     'D7','D8','D9','D10','D11','D12','D13'};
       PWM_PINS = {'D3','D5','D6','D9','D10','D11'};
       ANALOG_PINS = {'A0','A1','A2','A3','A4','A5'};
               
       % Color definitions
       DARK_RED = [0.3 0.05 0.05];
       DARK_BLUE = [0 0 0.2];
       BRIGHT_BLUE = [0.1 0.3 1];
   end
   
    methods
        %Constructor
        function self = ArduinoSimUltrasonic
            %Create the display
            self.hFigure=uifigure('Name','Arduino Simulator');
            self.hFigure.Position(3:4)=[300 400];
            
            % If we want to click and drag image instead of slider:
            %self.hFigure.WindowButtonDownFcn = self.createCallbackFcn(@ultrasonicDistance, true);
            
            % Create plot axes first to be behind other ui components
            % Shift negative to put exactly on figure edge, so plot
            % coordinates match figure coordinates.
            self.hAxes=uiaxes(self.hFigure,'Position',[-3 -3 300 200],'Color','none');
            self.hAxes.XTick = []; self.hAxes.YTick = [];
            self.hAxes.XColor = 'none'; self.hAxes.YColor = 'none';
            axis(self.hAxes, [0 300 0 200]);
            hold(self.hAxes,'on');
            
            % Position constants
            yMid = 60;
            xBtn = 50;
            xLED1 = 150;
            xLED2 = 200;
            
            % Components
            self.hButton=uibutton(self.hFigure,'state','Text','Button','Position',[xBtn yMid 45 20]);
            self.hLED1=uilamp(self.hFigure,'Color',self.DARK_BLUE,'Position',[xLED1 yMid 20 20]);
            self.hLED2=uilamp(self.hFigure,'Color',self.DARK_RED,'Position',[xLED2 yMid 20 20]);
            
            % Drawings & annotations
            plot(self.hAxes, xBtn+[23 23], yMid+[30 60],'k','LineWidth',2);
            uilabel(self.hFigure,'Text','D8','Position',[xBtn yMid+60 45 20],'HorizontalAlignment','center');
            uiimage(self.hFigure,'ImageSource','gnd.png','Position',[xBtn+5 20 35 30]);
            
            plot(self.hAxes, xLED1+[13 13], yMid+[30 100],'k','LineWidth',2);
            uiimage(self.hFigure,'ImageSource','res330.png','Position',[xLED1 yMid+40 20 40]);
            uilabel(self.hFigure,'Text','D2','Position',[xLED1 yMid+100 20 20],'HorizontalAlignment','center');
            uiimage(self.hFigure,'ImageSource','gnd.png','Position',[xLED1-8 20 35 30]);
            
            plot(self.hAxes, xLED2+[15 15], yMid+[30 100],'k','LineWidth',2);
            uiimage(self.hFigure,'ImageSource','res330.png','Position',[xLED2 yMid+40 20 40]);
            uilabel(self.hFigure,'Text','D12','Position',[xLED2-5 yMid+100 30 20],'HorizontalAlignment','center');
            uiimage(self.hFigure,'ImageSource','gnd.png','Position',[xLED2-7 20 35 30]);
            
            hold(self.hAxes,'off');
            
            % Ultrasonic slider and annotations
            self.Slider.X = 50; 
            self.Slider.Y = 250; 
            self.Slider.PixelRatio = 200;
            self.Slider.h = uislider(self.hFigure,...
                'Position',[self.Slider.X self.Slider.Y 200 3],...
                'Limits',[0 1],...
                'MajorTicks',[0 0.2 0.4 0.6 0.8 1],...
                'ValueChangingFcn',@(h,e) ultrasonicSliderChange(self,e));
            self.Slider.Value = self.Slider.h.Value;
            %self.hSlider.ValueChangingFcn = createCallbackFcn(self, @ultrasonicSliderChange, false);
            % Sensor image
            uiimage(self.hFigure,'ImageSource','ultrasonic.png',...
                'Position',[10 self.Slider.Y+10 40 100]);
            % Hand image
            self.Hand.Width = 30;
            self.Hand.Height = 80;
            self.Hand.h = uiimage(self.hFigure,'ImageSource','hand.png',...
                'Position',[(self.Slider.X + self.Slider.h.Value - self.Hand.Width/2)...
                             self.Slider.Y+10 self.Hand.Width self.Hand.Height]);
            
        end % constructor
        
        function writeDigitalPin(self,varargin)
            %Input check
            if length(varargin) < 2
                error('Not enough input arguments.');
            elseif length(varargin) > 2
                error('Too many input arguments.');
            elseif ~ishandle(self.hFigure)
                error('Error writing IOServerBlock. \nInvalid operation. Object must be connected to the serial port.',0);
            end
            pin = varargin{1};
            self.checkValidPin(pin)
            value = varargin{2};
            
            %set the LEDs
            switch lower(pin)
                case 'd2'
                    if value
                        self.hLED1.Color=self.BRIGHT_BLUE;
                        %self.hLED1.BackgroundColor=[0 0 1];
                        %self.hLED1.ForegroundColor=[0 0 0];
                    else
                        self.hLED1.Color=self.DARK_BLUE;
                        %self.hLED1.BackgroundColor=[0.1 0.1 0.4];
                        %self.hLED1.ForegroundColor=[0.8 0.8 0.8];
                    end
                case 'd12'
                    if value
                        self.hLED2.Color=[1 0 0];
                        %self.hLED2.BackgroundColor=[1 0 0];
                        %self.hLED2.ForegroundColor=[0 0 0];
                    else
                        self.hLED2.Color=self.DARK_RED;
                        %self.hLED2.BackgroundColor=[0.4 0.1 0.1];
                        %self.hLED2.ForegroundColor=[0.8 0.8 0.8];
                    end
            end                       
        end % writeDigitalPin
        
        function value = readDigitalPin(self,varargin)
            %Input check
            if length(varargin) < 1
                error('Not enough input arguments.');
            elseif length(varargin) > 1
                error('Too many input arguments.');
            elseif ~ishandle(self.hFigure)
                error('Error writing IOServerBlock. \nInvalid operation. Object must be connected to the serial port.',0);
            end
            pin = varargin{1};
            self.checkValidPin(pin);
            
            %Read the button
            switch lower(pin)
                case 'd8'
                    if self.isPullUp
                        value = double(~self.hButton.Value);
                    else
                        value = 0;
                    end
                otherwise
                    value = 0;
            end    
        end % readDigitalPin
        
        function configurePin(self,varargin)
            %Input check
            if length(varargin) < 2
                error('Not enough input arguments.');
            elseif length(varargin) > 2
                error('Too many input arguments.');
            elseif ~ishandle(self.hFigure)
                error('Error writing IOServerBlock. \nInvalid operation. Object must be connected to the serial port.',0);
            end
            pin = varargin{1};
            self.checkValidPin(pin);
            value = varargin{2};
            
            %set the LEDs
            switch lower(pin)
                case 'd8'
                    switch lower(value)
                        case "pullup"
                            self.isPullUp = 1;
                        otherwise
                            error('Invalid digital pin mode. The mode must be a character vector or string. Valid mode values are:\nTone, DigitalInput, DigitalOutput, Pullup, PWM, Servo, SPI, Interrupt, Ultrasonic, Unset.',0)
                    end
                otherwise
                
            end
        end % configurePin
        
        function sensorObj = ultrasonic(self, triggerPin, varargin)
            % Input check
            if ~ishandle(self.hFigure)
                error('Error writing IOServerBlock. \nInvalid operation. Object must be connected to the serial port.',0);
            end
            % check number of arguments
            if length(varargin) > 1
                error('Too many input arguments.');
            elseif length(varargin) == 1
                echoPin = varargin{1};
            else
                echoPin = triggerPin;
            end
            self.checkValidPin(triggerPin);
            self.checkValidPin(echoPin);
            
            % check that Ultrasonic library has been installed
            try
                if(any(ismember(self.Libraries, 'Ultrasonic')))
                    % If so, create sensor object
                    sensorObj = UltrasonicSim(self, triggerPin, echoPin);
                else
                    matlabshared.hwsdk.internal.localizedError('MATLAB:hwsdk:general:libraryNotUploaded', 'Ultrasonic');
                end
            catch e
                throwAsCaller(e);
            end

        end % ultrasonic
        

    end % methods
    
    methods (Access = private)
        
        function ultrasonicSliderChange(self,event)
            % Change the x-position of the "hand"
            self.Hand.h.Position(1) = ...
                self.Slider.X ...
                + (event.Value * self.Slider.PixelRatio) ...
                - (self.Hand.Width/2);
            % Update the store value as it changes
            self.Slider.Value = event.Value;
        end % ultrasonicSliderChange
        
        function checkValidPin(self,pin)
            if ~any([strcmp(pin,self.DIGITAL_PINS) strcmp(pin,self.ANALOG_PINS)])
                error('ArduinoSimUltrasonic:notValidPin','%s is not a valid pin.',pin)
            end
        end % checkValidPin
        
    end % methods - private
    
end % classdef
