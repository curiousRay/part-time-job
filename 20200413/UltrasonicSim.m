classdef UltrasonicSim < handle
    properties
        TriggerPin = [];
        EchoPin = [];
    end
    
    properties (Hidden)
        Parent = [];
    end
    
    methods
        % Constructor
        function self = UltrasonicSim(arduinoObj, triggerPin, echoPin)
            self.Parent = arduinoObj;
            self.TriggerPin = triggerPin;
            self.EchoPin = echoPin;
        end % constructor
        
        function value = readDistance(self)
            % Get slider value
            value = self.Parent.Slider.Value;
            
            % Add some randomness at small & large values
            if value < 0.02
                % Don't return value below 2cm
                value = Inf;
            elseif value < 0.05
                % Sometimes (50%) return value below 5cm
                if rand < 0.5
                    value = Inf;
                end
            elseif value > 0.95
                % Sometimes (30%) return value above 0.95m
                if rand < 0.7
                    value = Inf;
                end    
            elseif value > 0.9
                % Mostly (80%) return value above 0.9m
                if rand < 0.2
                    value = Inf;
                end
            end
                
            % Randomly (1%) return Inf
            if rand < 0.01  
                value = Inf;
            end
        end % readDistance
        
    end % methods
    
end % classdef