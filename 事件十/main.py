# -*- coding:utf-8 -*-
"""
@author: Lihao Lei
@license: GPL-3.0
@contact: leilei199708@gmail.com
@file: main.py
@desc: 去url指明的网址下载daily.json存放在同一目录下再运行
"""
import json
import datetime
import re
import csv
import numpy as np
import statsmodels.formula.api as smf
import pandas as pd
# import requests

url = 'https://api.covidtracking.com/v1/states/daily.json'
filename = 'daily.json'
i = 0
max_hashints = []
fips_dict = {
    'AL': 'Alabama', 'AK': 'Alaska', 'AZ': 'Arizona', 'AR': 'Arkansas', 'AS': 'American Samoa', 'CA': 'California',
    'CO': 'Colorado', 'CT': 'Connecticut', 'DC': 'Washington, D.C.', 'DE': 'Delaware', 'FL': 'Florida', 'GA': 'Georgia',
    'GU': 'Guam', 'HI': 'Hawaii', 'ID': 'Idaho', 'IL': 'Illinois', 'IN': 'Indiana', 'IA': 'Iowa', 'KS': 'Kansas',
    'KY': 'Kentucky', 'LA': 'Louisiana', 'ME': 'Maine', 'MD': 'Maryland', 'MA': 'Massachusetts', 'MI': 'Michigan',
    'MN': 'Minnesota', 'MO': 'Missouri', 'MP': 'Northern Mariana Islands', 'MS': 'Mississippi', 'MT': 'Montana',
    'NE': 'Nebraska', 'NV': 'Nevada', 'NH': 'New Hampshire', 'NJ': 'New Jersey', 'NM': 'New Mexico', 'NY': 'New York',
    'NC': 'North Carolina', 'ND': 'North Dakota', 'OH': 'Ohio', 'OK': 'Oklahoma', 'OR': 'Oregon', 'PA': 'Pennsylvania',
    'PR': 'The Commonwealth of Puerto Rico', 'RI': 'Rhode Island', 'SC': 'South Carolina', 'SD': 'South Dakota',
    'TN': 'Tennessee', 'TX': 'Texas', 'UT': 'Utah', 'VT': 'Vermont', 'VA': 'Virginia', 'VI': 'U.S. Virgin Islands',
    'WA': 'Washington', 'WV': 'West Virginia', 'WI': 'Wisconsin', 'WY': 'Wyoming'}
population_dict = {
    'AL': 4903185, 'AK': 731545, 'AZ': 7278717, 'AR': 3017804, 'AS': 49437, 'CA': 39512223,
    'CO': 5758736, 'CT': 3565287, 'DC': 705749, 'DE': 973764, 'FL': 21477737, 'GA': 10617423,
    'GU': 168485, 'HI': 1415872, 'ID': 168485, 'IL': 12671821, 'IN': 6732219, 'IA': 3155070, 'KS': 2913314,
    'KY': 4467673, 'LA': 4648794, 'ME': 1344212, 'MD': 6045680, 'MA': 6892503, 'MI': 9986857,
    'MN': 5639632, 'MO': 6137428, 'MP': 51433, 'MS': 2976149, 'MT': 1068778,
    'NE': 1934408, 'NV': 3080156, 'NH': 1359711, 'NJ': 8882190, 'NM': 2096829, 'NY': 19453561,
    'NC': 10488084, 'ND': 762062, 'OH': 11689100, 'OK': 3956971, 'OR': 4217737, 'PA': 12801989,
    'PR': 3193694, 'RI': 1059361, 'SC': 5148714, 'SD': 884659,
    'TN': 6829174, 'TX': 28995881, 'UT': 3205958, 'VT': 623989, 'VA': 8535519, 'VI': 106235,
    'WA': 7614893, 'WV': 1792147, 'WI': 5822434, 'WY': 578759
}


def filtdate(curr_date):
    curr_date = str(curr_date)
    date = datetime.date(int(curr_date[0:4]), int(curr_date[4:6]), int(curr_date[6:8]))
    if date.__lt__(datetime.date(2020, 12, 1)):
        return 1
    else:
        return 0


def get_max_hash(hash_str):
    max_int_str = re.findall('\d+', hash_str)
    return max(list(map(int, max_int_str)))


def get_deaths(fips):
    deaths = 0
    with open(filename, 'r') as f:
        file = json.load(f)
        for key in file:
            if key['date'] == 20201130 and key['state'] == fips and key['death'] is not None:
                deaths = key['death']
        f.close()
    return deaths


# req = requests.get(url)
# with open(filename, 'w') as f:
#     f.write(req.text)
#     f.close()

### 第一题开始
with open(filename, 'r') as f:
    obj = json.load(f)
    for key in obj:
        if filtdate(key['date']):
            i = i + 1
    f.close()

print(str(i))
### 第一题结束

### 第二题开始

with open(filename, 'r') as f:
    obj = json.load(f)
    for key in obj:
        max_hashints.append(get_max_hash(key['hash']))
    f.close()

print(max(max_hashints))
### 第二题结束
### 第三题开始

with open(filename, 'r') as f:
    with open("death.csv", 'w+', newline='') as cf:
        csv_write = csv.writer(cf)
        csv_write.writerow(['state', 'population', 'total_death', 'deaths_per.1million'])
        for state in fips_dict:
            print(state)
            csv_write.writerow([fips_dict[state],
                                population_dict[state],
                                get_deaths(state),
                                int(100000 * get_deaths(state) / population_dict[state]) + 1])
        cf.close()

    with open("death.csv") as cf:
        csv_read = csv.DictReader(cf)
        result_col = list(map(float, [row['deaths_per.1million'] for row in csv_read]))
        max_ave_population = max(result_col)

        state_list = []
        for key, val in fips_dict.items():
            state_list.append(val)
        print("%s has the highest deaths per capita with %d deaths among 100,000 people." %
              (state_list[result_col.index(max_ave_population)],
               max_ave_population))

        cf.close()

    f.close()

### 第三题结束
### 第四题开始

i = 0
MAdeathIncrease = np.zeros(314)
MAhospitalizedIncrease = np.zeros(314)
MApositiveIncrease = np.zeros(314)

with open(filename, 'r') as f:
    obj = json.load(f)
    for key in obj:
        if filtdate(key['date']) and key['state'] == 'MA':
            MAdeathIncrease[i] = key['deathIncrease']
            MAhospitalizedIncrease[i] = key['hospitalizedIncrease']
            MApositiveIncrease[i] = key['positiveIncrease']
            i = i + 1
    f.close()

learningRate = 1.0e-9
numIterations = 1000
x = np.array([MAhospitalizedIncrease, MApositiveIncrease])
# xb = sm.add_constant(X)
y = MAdeathIncrease.reshape(-1, 1)
m = len(y)  # number of samples
theta = np.array([[1], [1]])

# X = sm.add_constant(np.column_stack((x[0], x[1])))
X = np.column_stack((x[0], x[1]))
xb = X  # no intercept term
# start gradient descent
for i in range(numIterations):
    gradient = 2 / m * xb.T.dot(xb.dot(theta) - y)  # dimension: (2,1)
    theta = theta - learningRate * gradient

# print(theta)
print("Estimated coefficient of x1 is %f." % theta[0])

beta = [theta[0], theta[1], -1]

# y = np.dot(X, beta)
# results = sm.OLS(y, X).fit()

data = pd.DataFrame({'x1': MAhospitalizedIncrease,
                     'x2': MApositiveIncrease,
                     'y': MAdeathIncrease})

results = smf.ols(formula='y ~ x1 + x2 - 1', data=data).fit()
# print(results.summary())
print("The actual least squares coefficient of x1 is %f." % results.params[0])

### 第四题结束