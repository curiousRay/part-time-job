function [ dist ] = yuran_DistFunct(lat1, long1, lat2, long2)
% `lat` and `lon` are both in degrees.
% `dist` is in meters.

radEarth = 6371000;

delta_lat = deg2rad(lat2 - lat1);
delta_long = deg2rad(long2 - long1);

a = (sin(delta_lat / 2))^2 + cos(lat1) * cos(lat2) * (sin(delta_long / 2))^2;
c = 2 * atan2(sqrt(a), sqrt(1 - a));
dist = radEarth * c;

end

