%% MATLAB Practical Template Main Script File <----ERASE PRIOR TO SUBMISSION
%% (STANDARD COURSE HEADER)<---replace prior to submission

%% Data loading section

clear all;clc;

filename = input('Plz input dataset filename:');
% I use 'Materials Needed Dataset-E.xlsx';
if ~exist(filename, 'file')
   error(['No such file named ', filename, '.']);
    return
end

T = readtable(filename, 'ReadVariableNames', 1, 'FileType', 'spreadsheet');
% `T` means table
size = size(T, 1);
% total number of recordings

datacell = table2array(T);
% convert table to cell

for i = 1:size
    cellrow = regexp(char(datacell(i)), ',', 'split');
    % `cellrow` is the cursor of `datacell`.
    
    time = str2double(cellrow{1});
    lat = str2double(cellrow{2});
    long = str2double(cellrow{3});
    datacell(i,1:3) = {time, lat, long};
    % change `datacell` to 3 columns.
end

%% Function call section

total_dist = 0;
% total dist is in meters.

for j = 1:size - 1
    delta_dist = yuran_DistFunct(datacell{j, 2}, datacell{j, 3}, datacell{j + 1, 2}, datacell{j + 1, 3});
    total_dist = total_dist + delta_dist;
end

%% Average speed calculation section

shift = yuran_DistFunct(datacell{size, 2}, datacell{size, 3}, datacell{1, 2}, datacell{1, 3});
time_interval = datacell{size, 1} - datacell{1, 1};
average_speed = shift / time_interval;

%% Max and min calculation section

speeds = zeros(1, size - 1);
max = 0;
min = inf;

for k = 1:length(speeds)
    delta_dist = yuran_DistFunct(datacell{k, 2}, datacell{k, 3}, datacell{k + 1, 2}, datacell{k + 1, 3});
    speeds(k) = delta_dist / (datacell{k + 1, 1} - datacell{k, 1});
    % calculate speeds
    
    if isnan(speeds(k))
        speeds(k) = speeds(k - 1);
        % replace NaN speeds.
    elseif speeds(k) >= max
        max = speeds(k);
        elseif speeds(k) <= min
            min = speeds(k);
            % get max speed and min speed
    end
end

%% Standard deviation calculation section

speeds_sum = 0;

for l = 1:length(speeds)
    speeds_sum = speeds_sum + speeds(l); 
end
% calculate sum of instantaneous speeds.

speeds_average = speeds_sum / length(speeds);
sum = 0;
% `sum` is used to get standard deviation.

for m = 1: length(speeds)
    sum = sum + (speeds(m) - speeds_average)^2;
end

standard_deviation = sqrt(sum / length(speeds));

%% Output calculation section

fid=fopen('yuran_RaceData.txt', 'w');
fprintf(fid, 'Over the course of the race, the average speed of the boat was %.7f m/s, ', average_speed);
fclose(fid);
% use write mode to clean up contents in every launch.

fid=fopen('yuran_RaceData.txt', 'at+');
% use append mode for exploded sentences.
fprintf(fid, 'the total distance traveled was %.6f km, ', total_dist / 1000);
fprintf(fid, 'the maximum instantaneous speed was %.7f m/s, ', max);
fprintf(fid, 'the minimum instantaneous speed was %.7f m/s, ', min);
fprintf(fid, 'and the standard deviation of the speed was %.7f.', standard_deviation);
fclose(fid);
