<?php
  include "inc/html-top.php";
?>

<body>

<?php
  include "inc/nav.php";
?>

<main>


<div class="container">
	<section class="briefintro">
	<h2 class="h21" id="main">Brief Introduction</h2>

	<div class="paraset1">
		<p>Charles Babbage(26 December 1791 – 18 October 1871) was an English polymath.[1] A mathematician, philosopher, inventor and mechanical engineer, Babbage originated the concept of a digital programmable computer.
		</p>
  		<p>Considered by some to be a "father of the computer", Babbage is credited with inventing the first mechanical computer that eventually led to more complex electronic designs, though all the essential ideas of modern computers are to be found in Babbage's analytical engine. His varied work in other fields has led him to be described as "pre-eminent" among the many polymaths of his century.</p>
		<p>Parts of Babbage's incomplete mechanisms are on display in the Science Museum in London. In 1991, a functioning difference engine was constructed from Babbage's original plans. Built to tolerances achievable in the 19th century, the success of the finished engine indicated that Babbage's machine would have worked.</p>
	</div>

	<figure class="img1">
		<img src="images/charles1.jpg" alt="Charles-Babbage">
		<figcaption>Charles Babbage</figcaption>
	</figure>
	</section>


	<div class="section2">
	<h2 class="h22" id="early">Early life</h2>
		<div class="p21">
		<p>Babbage's birthplace is disputed, but according to the Oxford Dictionary of National Biography he was most likely born at 44 Crosby Row, Walworth Road, London, England.[7] A blue plaque on the junction of Larcom Street and Walworth Road commemorates the event.</p>
		<p>His date of birth was given in his obituary in The Times as 26 December 1792; but then a nephew wrote to say that Babbage was born one year earlier, in 1791. The parish register of St. Mary's, Newington, London, shows that Babbage was baptised on 6 January 1792, supporting a birth year of 1791.</p>
	</div>

	<div class="p22">
		<p>Babbage was one of four children of Benjamin Babbage and Betsy Plumleigh Teape. His father was a banking partner of William Praed in founding Praed's & Co. of Fleet Street, London, in 1801. In 1808, the Babbage family moved into the old Rowdens house in East Teignmouth. Around the age of eight, Babbage was sent to a country school in Alphington near Exeter to recover from a life-threatening fever. For a short time he attended King Edward VI Grammar School in Totnes, South Devon, but his health forced him back to private tutors for a time.</p>
		<p>Babbage then joined the 30-student Holmwood Academy, in Baker Street, Enfield, Middlesex, under the Reverend Stephen Freeman. The academy had a library that prompted Babbage's love of mathematics. He studied with two more private tutors after leaving the academy. The first was a clergyman near Cambridge; through him Babbage encountered Charles Simeon and his evangelical followers, but the tuition was not what he needed.[14] He was brought home, to study at the Totnes school: this was at age 16 or 17. The second was an Oxford tutor, under whom Babbage reached a level in Classics sufficient to be accepted by Cambridge.</p>
	</div>
	</div>
</div><!--container-->


<div class="gridbox1">

	<h2 class="h23" id="career">Life at the University of Cambridge</h2>
		<div class="box1">
		<p>Babbage arrived at Trinity College, Cambridge, in October 1810.He was already self-taught in some parts of contemporary mathematics; he had read in Robert Woodhouse, Joseph Louis Lagrange, and Marie Agnesi. As a result, he was disappointed in the standard mathematical instruction available at the university.</p>
	</div>

	<div class="box2">
		<p>Babbage, John Herschel, George Peacock, and several other friends formed the Analytical Society in 1812; they were also close to Edward Ryan. As a student, Babbage was also a member of other societies such as The Ghost Club, concerned with investigating supernatural phenomena, and the Extractors Club, dedicated to liberating its members from the madhouse, should any be committed to one.</p>
	</div>


	<div class="box3">
		<p>In 1812 Babbage transferred to Peterhouse, Cambridge. He was the top mathematician there, but did not graduate with honours. He instead received a degree without examination in 1814. He had defended a thesis that was considered blasphemous in the preliminary public disputation; but it is not known whether this fact is related to his not sitting the examination.</p>
	</div>
</div>



<div class="container2">
	<div class="gridbox2">
	<h2 class="h24" id="contributions">Life after Cambridge</h2>

		<div class="grid1">
		<p>Considering his reputation, Babbage quickly made progress. He lectured to the Royal Institution on astronomy in 1815, and was elected a Fellow of the Royal Society in 1816. After graduation, on the other hand, he applied for positions unsuccessfully, and had little in the way of career. In 1816 he was a candidate for a teaching job at Haileybury College; he had recommendations from James Ivory and John Playfair, but lost out to Henry Walter. In 1819, Babbage and Herschel visited Paris and the Society of Arcueil, meeting leading French mathematicians and physicists. That year Babbage applied to be professor at the University of Edinburgh, with the recommendation of Pierre Simon Laplace; the post went to William Wallace.</p>
	</div>

	<div class="grid2">
		<p>With Herschel, Babbage worked on the electrodynamics of Arago's rotations, publishing in 1825. Their explanations were only transitional, being picked up and broadened by Michael Faraday. The phenomena are now part of the theory of eddy currents, and Babbage and Herschel missed some of the clues to unification of electromagnetic theory, staying close to Ampère's force law.</p>
	</div>

	<div class="grid3">
		<p>Babbage purchased the actuarial tables of George Barrett, who died in 1821 leaving unpublished work, and surveyed the field in 1826 in Comparative View of the Various Institutions for the Assurance of Lives. This interest followed a project to set up an insurance company, prompted by Francis Baily and mooted in 1824, but not carried out. Babbage did calculate actuarial tables for that scheme, using Equitable Society mortality data from 1762 onwards. </p>
	</div>

	<div class="grid4">
		<p>During this whole period Babbage depended awkwardly on his father's support, given his father's attitude to his early marriage, of 1814: he and Edward Ryan wedded the Whitmore sisters. He made a home in Marylebone in London, and founded a large family. On his father's death in 1827, Babbage inherited a large estate (value around £100,000, equivalent to £8.5 million or $11.3 million today), making him independently wealthy. After his wife's death in the same year he spent time travelling. In Italy he met Leopold II, Grand Duke of Tuscany, foreshadowing a later visit to Piedmont. In April 1828 he was in Rome, and relying on Herschel to manage the difference engine project, when he heard that he had become professor at Cambridge, a position he had three times failed to obtain (in 1820, 1823 and 1826).</p>
	</div>
</div>



<div class="gridbox3">
		
	<div class="awards">

	<h2 class="h27" id="awards">Family</h2>		
		<figure>
			<img src="images/charles2.jpg" alt="Charles Babbage">
			<figcaption>later life</figcaption>
		</figure>
		<ul>
			<li>Benjamin Herschel Babbage (1815-1878)</li>
			<li>Charles Whitmore Babbage (1817-1827)</li>
			<li>Georgiana Whitmore Babbage (1818-??)</li>
			<li>Edward Stewart Babbage (1819-1821)</li>
			<li>Francis Moore Babbage (1821-??)</li>
			<li>Dugald Bromhead (Bromheald?) Babbage (1823-1901)</li>
			<li>Alexander Forbes Babbage (1827–1827)</li>
		</ul>
	</div>


	<div class="personal">
		<h2 class="h25" id="personal">Later life</h2>

		<p>The British Association was consciously modelled on the Deutsche Naturforscher-Versammlung, founded in 1822. It rejected romantic science as well as metaphysics, and started to entrench the divisions of science from literature, and professionals from amateurs. Belonging as he did to the "Wattite" faction in the BAAS, represented in particular by James Watt the younger, Babbage identified closely with industrialists. He wanted to go faster in the same directions, and had little time for the more gentlemanly component of its membership. Indeed, he subscribed to a version of conjectural history that placed industrial society as the culmination of human development (and shared this view with Herschel). A clash with Roderick Murchison led in 1838 to his withdrawal from further involvement. At the end of the same year he sent in his resignation as Lucasian professor, walking away also from the Cambridge struggle with Whewell. His interests became more focussed, on computation and metrology, and on international contacts.</p>
		<p>A project announced by Babbage was to tabulate all physical constants (referred to as "constants of nature", a phrase in itself a neologism), and then to compile an encyclopaedic work of numerical information. He was a pioneer in the field of "absolute measurement". His ideas followed on from those of Johann Christian Poggendorff, and were mentioned to Brewster in 1832. There were to be 19 categories of constants, and Ian Hacking sees these as reflecting in part Babbage's "eccentric enthusiasms". Babbage's paper On Tables of the Constants of Nature and Art was reprinted by the Smithsonian Institution in 1856, with an added note that the physical tables of Arnold Henry Guyot"will form a part of the important work proposed in this article".</p>
		<p>As early as 1845, Babbage had solved a cipher that had been posed as a challenge by his nephew Henry Hollier, and in the process, he made a discovery about ciphers that were based on Vigenère tables. Specifically, he realized that enciphering plain text with a keyword rendered the cipher text subject to modular arithmetic. During the Crimean War of the 1850s, Babbage broke Vigenère's autokey cipher as well as the much weaker cipher that is called Vigenère cipher today. His discovery was kept a military secret, and was not published. Credit for the result was instead given to Friedrich Kasiski, a Prussian infantry officer, who made the same discovery some years later. However, in 1854, Babbage published the solution of a Vigenère cipher, which had been published previously in the Journal of the Society of Arts. In 1855, Babbage also published a short letter, "Cypher Writing", in the same journal. Nevertheless, his priority was not established until 1985.</p>
	</div>



</div>


	<h2 class="h26" id="development">Publications</h2>
		<table class="charles">
			<tr>
				<th>Date</th>
				<th>Name</th>
				<th>issue comments</th>
			</tr>
			<tr>
				<td>1826</td>
				<td>A Comparative View of the Various Institutions for the Assurance of Lives</td>
				<td>London: J. Mawman</td>
			</tr>
			<tr>
				<td>1830</td>
				<td>Reflections on the Decline of Science in England, and on Some of Its Causes</td>
				<td>London: B. Fellowes</td>
			</tr>
			<tr>
				<td>1835</td>
				<td>On the Economy of Machinery and Manufactures (4th ed)</td>
				<td>London: Charles Knight</td>
			</tr>
			<tr>
				<td>1837</td>
				<td>The Ninth Bridgewater Treatise, a Fragment</td>
				<td>London: John Murray</td>
			</tr>
			<tr>
				<td>1851</td>
				<td>The Exposition of 1851</td>
				<td>London: John Murray</td>
			</tr>
		</table>
</div>
	</main>

	<footer>

	<h2 class="h28">Citations</h2>

	<ul class="citation">

		<li><a href="https://plato.stanford.edu/entries/computing-history/">
		"The Modern History of Computing". </a></li>

		<li><a href="http://www.cbi.umn.edu/about/babbage.html">WHO WAS CHARLES BABBAGE?</a></li>

		<li><a href="https://books.google.com/books?id=PKERMZkA9A0C&pg=PA247#v=onepage&q&f=false"> Genesis and Geology: A Study in the Relations of Scientific Thought, Natural Theology</a></li>
	<div class="footp">
		<p>CSC 170 Webpage Design and Development</p>
		<p>Original Source: <a href="http://csc170.org/yzhai9/project1/">yzhai9</a></p>
		<p>@2019 Boqing Zheng</p>
	</div>

	</ul>
	</footer>

<?php include "inc/scripts.php"; ?>

</body>
</html>