<?php
  include "inc/html-top.php";
?>

<body>

<?php
  include "inc/nav.php";
?>

<div class="container">

<h1 class="home-title">Project 3 - Pioneers of Internet</h1>

<div class="slider">
	<img class="flexible" src="images/boas.png" alt="Franz Boas">
	<img class="flexible" src="images/larry1.jpg" alt="Lawrence Roberts" >
	<img class="flexible" src="images/charles1.jpg" alt="Charles Babbage">
	<img class="flexible" src="images/tim.jpg" alt="Tim Berners-Lee">
</div>

<h2>Lawrence Roberts</h2>	
<section class="intro">
	<div class="part1">
		<p>
			Lawrence Gilman Roberts (December 21, 1937 – December 26, 2018) was an American engineer who received the Draper Prize in 2001 "for the development of the Internet", and the Principe de Asturias Award in 2002.As a program manager and office director at the Advanced Research Projects Agency, Roberts and his team created the ARPANET using packet switching techniques invented by British computer scientist Donald Davies and American Paul Baran underpinned by the theoretical work of Leonard Kleinrock. The ARPANET, which was built by the Massachusetts-based company Bolt Beranek and Newman (BBN), was a predecessor to the modern Internet. He later served as CEO of the commercial packet-switching network Telenet.
		</p>
	</div>

	<div>
		<img src="images/larry1.jpg" alt="Lawrence Roberts" >
	</div>
</section>


<h2>Franz Boas</h2>
<section class="intro">
	<div class="part1">
		<p>Studying in Germany, Boas was awarded a doctorate in 1881 in physics while also studying geography. He then participated in a geographical expedition to northern Canada, where he became fascinated with the culture and language of the Baffin Island Inuit. He went on to do field work with the indigenous cultures and languages of the Pacific Northwest. In 1887 he emigrated to the United States, where he first worked as a museum curator at the Smithsonian, and in 1899 became a professor of anthropology at Columbia University, where he remained for the rest of his career. </p>
	</div>

	<div>
		<img src="images/boas.png" alt="Franz Boas">
	</div>
</section>



<h2>Charles Babbage</h2>
<section class="intro">
	<div class="part1">
		<p>Charles Babbage(26 December 1791 – 18 October 1871) was an English polymath. A mathematician, philosopher, inventor and mechanical engineer, Babbage originated the concept of a digital programmable computer.
  		Considered by some to be a "father of the computer", Babbage is credited with inventing the first mechanical computer that eventually led to more complex electronic designs, though all the essential ideas of modern computers are to be found in Babbage's analytical engine. His varied work in other fields has led him to be described as "pre-eminent" among the many polymaths of his century.Parts of Babbage's incomplete mechanisms are on display in the Science Museum in London. In 1991, a functioning difference engine was constructed from Babbage's original plans. Built to tolerances achievable in the 19th century, the success of the finished engine indicated that Babbage's machine would have worked.</p>
	</div>
	<div>
		<img src="images/charles1.jpg" alt="Charles Babbage">
	</div>
</section>


<h2>Tim Berners-Lee</h2>
<section class="intro">
	<div class="part1">
		<p>Elon Reeve Musk FRS ( born June 28, 1971) is a technology entrepreneur, investor, and engineer. He holds South African, Canadian, and U.S. citizenship and is the founder, CEO, and chief engineer/designer of SpaceX; co-founder, CEO, and product architect of Tesla, Inc.; co-founder of Neuralink; founder of The Boring Company;co-founder and initial co-chairman of OpenAI;and co-founder of PayPal. In December 2016, he was ranked 21st on the Forbes list of The World's Most Powerful People. He has a net worth of $19.4 billion and is listed by Forbes as the 40th-richest person in the world.</p>
		
	</div>
	<div>
		<img src="images/tim.jpg" alt="Tim Berners-Lee">	
	</div>
</section>


<footer>
		<div class="footp">
		<p>CSC 170 Webpage Development</p>
		<p>@2019 Boqing Zheng</p>
	</div>
</footer>

</div>

<?php include "inc/scripts.php"; ?>

	<script src="sss/sss.js"></script>

	<script>
	$('.slider').sss();
	</script>


</body>
</html>