<!DOCTYPE html>
<html lang="en">

<head>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-153874524-2"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
 	 	function gtag(){dataLayer.push(arguments);}
 	 	gtag('js', new Date());
		gtag('config', 'UA-153874524-2');
	</script>

	<meta charset="UTF-8">
	<title>Boqing Zheng | CSC 170 Project 3</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet"  href="css/styles.css">
	<link rel="stylesheet"  href="css/nav.css">
	<link rel="stylesheet"  href="css/home.css">
	<link rel="stylesheet"  href="sss/sss.css">
</head>