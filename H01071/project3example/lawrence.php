<?php
  include "inc/html-top.php";
?>

<body>

<?php
  include "inc/nav.php";
?>

<main>


<div class="container">
	<section class="briefintro">	
	<h2 class="h21" id="main">Brief Introduction</h2>

	<div class="paraset1">
	<p>Lawrence Gilman Roberts (December 21, 1937 – December 26, 2018) was an American engineer who received the Draper Prize in 2001 "for the development of the Internet", and the Principe de Asturias Award in 2002.</p>

	<p>As a program manager and office director at the Advanced Research Projects Agency, Roberts and his team created the ARPANET using packet switching techniques invented by British computer scientist Donald Davies and American Paul Baran underpinned by the theoretical work of Leonard Kleinrock. The ARPANET, which was built by the Massachusetts-based company Bolt Beranek and Newman (BBN), was a predecessor to the modern Internet. He later served as CEO of the commercial packet-switching network Telenet.</p>

	<p>While Berners-Lee is known as the father of the World Wide Web, Lawrence Roberts is one of those folks acknowledged for siring the internet as a whole. Building on Kleinrock’s packet switching technology, Roberts designed the ARPANET, an advanced research project on behalf of the US Department of Defense. The idea behind the project was to interconnect computers around the nation, and in November 1969 the first successful communications were sent from a computer at the University of California in Los Angeles to one at the Stanford Research Institute – 350 miles away.</p>
	</div>
	

	<figure class="img1">
		<img src="images/larry1.jpg" alt="Lawrence-Roberts" >
		<figcaption>Lawrence Gilman Roberts</figcaption>
	</figure>
	</section>



<div class="section2">
	<h2 class="h22" id="early">Early Life and Education</h2>


		<p class="p21">Roberts, who was known as Larry, was born and raised in Westport, Connecticut. He was the son of Elizabeth (Gilman) and Elliott John Roberts, both of whom had doctorates in chemistry. During his youth, he built a Tesla coil, assembled a television, and designed a telephone network built from transistors for his parents' Girl Scout camp.These days, other names like Vint Cerf get more credit in the popular press for the development of the internet. Perhaps because unlike the notoriously publicity-hungry Cerf.</p>

		<p class="p22">Roberts attended the Massachusetts Institute of Technology (MIT), where he received his bachelor's degree (1959), master's degree (1960), and Ph.D. (1963), all in electrical engineering.  Roberts didn’t have the same flair for self-promotion as some other internet pioneers. But without Larry Roberts and his determined vision for what networks could do, the history of the internet would’ve certainly looked very different.His Ph.D. thesis "Machine Perception of Three-Dimensional Solids" is considered as one of the foundational works of the field of Computer Vision.</p>

	</div>
</div><!--container-->


<div class="gridbox1">
	<h2 class="h23" id="career">Career</h2>

		<div class="box1">
			<h3>Getting into ARPANET</h3>		

			<p>In 1967, he was recruited by Robert Taylor in the ARPA Information Processing Techniques Office (IPTO) to become the program manager for the ARPANET. Wesley A. Clark suggested the use of a dedicated computer, called the Interface Message Processor at each node of the network instead of centralized control. Shortly afterwards, at the 1967 ACM Symposium on Operating System Principles, Roberts met a member of Donald Davies's team (Roger Scantlebury) who presented their research on packet switching and suggested it for use in the ARPANET. </p>
			<figure>
				<img src="images/arpanet.jpg" alt="Arpanet">
				<figcaption>ARPANET</figcaption>
			</figure>

			<p>Roberts applied Davies's concepts of packet switching for the ARPANET, and sought input from Paul Baran and Leonard Kleinrock. Subsequently, Roberts developed the plan for the ARPANET, the first wide area packet-switching network with distributed control across multiple computers, and managed its implementation. ARPA issued a request for quotation (RFQ) to build the system, which was awarded to Bolt, Beranek and Newman (BBN). When Robert Taylor was sent to Vietnam in 1969 and then resigned, Roberts became director of the IPTO.</p>
		</div>



	<div class=box2>		
	<h3>Early Career</h3>
			<p>After receiving his PhD, Roberts continued to work at the MIT Lincoln Laboratory. Having read the seminal 1961 paper of the "Intergalactic Computer Network" by J. C. R. Licklider, Roberts developed a research interest in time-sharing using computer networks.</p>

			<p>In the late 1960s, Roberts was manager of the Pentagon’s Advanced Research Projects Agency, ARPA (now known as DARPA), and oversaw the development of the ARPANET, the precursor to the modern internet. The ARPANET’s first host-to-host connection on October 29, 1969 from UCLA to the Stanford Research Institute were the internet’s first baby steps.</p>			


			<p>For instance, just a week before the ARPANET made its first host-to-host connection in 1969, Roberts and fellow internet pioneer J.C.R. Licklider gave a presentation to the National Security Agency (NSA) in Fort Meade, Maryland. We don’t know exactly what they told the NSA, but we do know their topic of conversation: What networks would look like ten years into the future, in 1979. Larry’s job was about looking to the future on behalf of the Pentagon, a connection to the true military purpose of the early internet that many others have disingenuously shied away from.</p>
	</div>



	<div class="box3">
		<h3>Later Career</h3>

			<p>In 1973, Roberts left ARPA to commercialize the nascent packet-switching technology in the form of Telenet, the first packet switch utility company, and served as its CEO from 1973 to 1980. In 1983 he joined DHL Corporation as President and CEO. He was CEO of NetExpress, an Asynchronous Transfer Mode (ATM) equipment company, from 1983 to 1993. Roberts was president of ATM Systems from 1993 to 1998. He was chairman and CTO of Caspian Networks, but left in early 2004; Caspian ceased operation in late 2006.</p>
			<figure class="img2">
				<img src="images/speech.png" alt="speech">
				<figcaption>Larry gave a speech</figcaption>
			</figure>
			<p>As of 2011, Roberts was the founder and chairman of Anagran Inc. Anagran continues work in the same area as Caspian: IP flow management with improved quality of service for the Internet. Since September 2012, he was CEO of Netmax in Redwood City, California.</p>

			<p>The work that Roberts contributed to the development of the internet is largely thought of as administrative and managerial as opposed to strictly technical, but there are many choices that Roberts made about the design of the internet that will likely take decades to become declassified and known to the public.</p>
</div>
</div><!--box2-->



<div class="container2">
	<div class="gridbox2">
	<h2 class="h24" id="contributions">Contributions</h2>	

		<div class="grid1">
		<h3>Arpanet</h3>

			<p>He connected all ARPA-sponsored computers directly over dial-up telephone lines. Networking functions would be handled by "host" computers at each site. This idea was not well-received. Researchers did not want to relinquish valuable computing resources to administer this new network and did not see how they would benefit from sharing resources with other researchers.</p>

			<p>also foresaw problems trying to facilitate communication between machine with many different incompatible operating systems and languages. All in all the reception to Roberts' plans was a cold one.</p>

			<p>ARPANET was the network that became the basis for the Internet. Based on a concept first published in 1967, ARPANET was developed under the direction of the U.S. Advanced Research Projects Agency (ARPA). In 1969, the idea became a modest reality with the interconnection of four university computers. The initial purpose was to communicate with and share computer resources among mainly scientific users at the connected institutions. ARPANET took advantage of the new idea of sending information in small units called packets that could be routed on different paths and reconstructed at their destination. The development of the TCP/IP protocols in the 1970s made it possible to expand the size of the network, which now had become a network of networks, in an orderly way.</p>
			<figure>
				<img src="images/arpanet2.jpg" alt="Arpanet">
				<figcaption>ARPANET</figcaption>
			</figure>
		</div>


		<div class="grid2">
		<h3>IMPs</h3>
			<p>All of the small computers could speak the same language which would facilitate communication between them. Each host computer would only have to adapt its language once in communicating with it's small computer counterpart. Each host computer would be connected to the network via its small computer which would act as a sort of gateway. The small computers could also remain under more direct ARPA control than were the large host computers.He called the small computers Interface Message Processors</p>

			<p>The Interface Message Processor (IMP) was the packet switching node used to interconnect participant networks to the ARPANET from the late 1960s to 1989. It was the first generation of gateways, which are known today as routers.vAn IMP was a ruggedized Honeywell DDP-516 minicomputer with special-purpose interfaces and software. In later years the IMPs were made from the non-ruggedized Honeywell 316 which could handle two-thirds of the communication traffic at approximately one-half the cost. An IMP requires the connection to a host computer via a special bit-serial interface, defined in BBN Report 1822. The IMP software and the ARPA network communications protocol running on the IMPs was discussed in RFC 1, the first of a series of standardization documents published by the Internet Engineering Task Force (IETF).</p>
		</div>



		<div class="grid3">
		<h3>Telenet</h3>
			<p>After ARPA, Dr. Roberts founded the world's first packet data communications carrier, Telenet - the company that developed and drove adoption of the popular X.25 data protocol. Roberts was CEO from 1973 to 1980. Telenet was sold to GTE in 1979 and subsequently became the data division of Sprint. From 1983 to 1993, Roberts was Chairman and CEO of NetExpress, an electronics company specializing in packetized fax and ATM equipment. Roberts was president of ATM Systems from 1993 to 1998.</p>

			<p>Telenet was an American commercial packet switched network which went into service in 1974. It was the first packet-switched network service that was available to the general public. Various commercial and government interests paid monthly fees for dedicated lines connecting their computers and local networks to this backbone network. Free public dialup access to Telenet, for those who wished to access these systems, was provided in hundreds of cities throughout the United States.</p>
		</div>



		<div class="grid4">
		<h3>Economic flow management system</h3>
			<p>In 2004 Dr. Roberts founded Anagran Inc. Realizing that the output queue design of packet switches and routers was causing major delay, packet loss, and unfairness, and he designed a new concept of flow management where each flow is precisely rate controlled at the input rather than randomly at the output. The flow manager concept only required 20% of the power and size of a L3 router, virtually eliminated queuing delay and packet loss for both file transfers and streaming media, optimized network utilization, and greatly improved fairness. Thus, instead of the prevailing concept that Quality of Service (QoS) would increase the complexity and cost of a network, the QoS could be greatly improved with less complexity while at the same time reducing network cost by eliminating the need for overcapacity.</p>

		</div>
		</div><!--gridbox-->

<div class="gridbox3">
	<div class="personal">
	<h2 class="h25" id="personal">Personal Life</h2>
		<figure>
			<img src="images/earlylarry.jpeg" alt="Early Lawrence-Roberts">
			<figcaption>Early Year</figcaption>
		</figure>
	
		<p>Roberts married and divorced four times. At the time of his death, his partner was physician Tedde Rinker. Roberts died at his California home from a heart attack on December 26, 2018.</p>
		<p>Interestingly, many people at universities around the country were initially very skeptical about the promise of networked computers and Roberts essentially had to talk them into trying out the new technology.</p>

		<p>“Although they knew in the back of their mind that it was a good idea and were supportive on a philosophical front, from a practical point of view, they—Minsk and McCarthy [two prominent computer scientists], and everybody with their own machine-wanted [to continue having] their own machine,” Roberts said in a 1989 interview published at the Computer History Museum’s website.“</p>
	</div>

	<div class="awards">
	<h2 class="h27" id="awards">Awards and honors</h2>
	<ul>
		<li>IEEE Harry H. Goode Memorial Award (1976):In recognition of his contributions to the architectural design of computer-communication systems, his leadership in creating a fertile research environment leading to advances in computer and satellite communications techniques, his role in the establishment of standard international communication protocols and procedures, and his accomplishments in development and demonstration of packet switching technology and the ensuing networks which grew out of this work.</li>
		<li>L.M. Ericsson Prize (1982) in Sweden</li>
		<li>Member, National Academy of Engineering (1978)</li>
		<li>Computer Design Hall of Fame Award (1982)</li>
		<li>IEEE W. Wallace McDowell Award (1990):For architecting packet switching technology and bringing it into practical use by means of the ARPA network.</li>
		<li>Association for Computing Machinery SIGCOMM Award (1998):visionary contributions and advanced technology development of computer communication networks".</li>
		<li>IEEE Internet Award (2000):early, preeminent contributions in conceiving, analyzing and demonstrating packet-switching networks, the foundation technology of the Internet.</li>
		<li>International Engineering Consortium Fellow Award (2001)</li>
		<li>National Academy of Engineering Charles Stark Draper Prize (2001)</li>
		<li>Principe de Asturias Award 2002 in Spain: or designing and implementing a system that is changing the world by providing previously unthought of opportunities for social and scientific progress.</li>
		<li>NEC C&C Award (2005) in Japan: For Contributions to Establishing the Foundation of Today's Internet Technology through the Design and Development of ARPANET and Other Early Computer Networks that were Part of the Initial Internet.</li>
		<li>Internet Hall of Fame by the Internet Society (2012)</li>
	</ul>
	</div>
</div>




	<h2 class="h26" id="development">The Development of Internet</h2>
		<table class="development">
  		<tr>
    		<th>Year</th>
    		<th>Person</th>
    		<th>Work</th>
  		</tr>
 		<tr>
   			<td>1960s</td>
    		<td>Leonard Kleinrock</td>
    		<td>Packet switching</td>
  		</tr>
 		<tr>
    		<td><strong><em>1970s</em></strong></td>
    		<td><strong><em>Larry Roberts</em></strong></td>
    		<td><strong><em>ARPAnet</em></strong></td>
  		</tr>
  		<tr>
  			<td>1970s</td>
  			<td>Vint Cerf and Robert Kahn</td>
  			<td>TCP/IP</td>
  		</tr>
  		<tr>
  			<td>1970s</td>
  			<td>Raymond Tomlinson</td>
  			<td>Email</td>
  		</tr>
  		<tr>
  			<td>1980s</td>
  			<td>Paul Mockapetris and John Postel</td>
  			<td>Domain Name System</td>
  		</tr>
  		<tr>
  			<td>1990s</td>
  			<td>Sir Tim Berners-Lee</td>
  			<td>The World Wide Web</td>
  		</tr>
	</table>
	
</div>


</main>


	<footer>
	<h2 class="h28">Citations</h2>
	<ul class="citation">
		<li><a href="https://galeapps.gale.com/apps/auth?userGroupName=fairfax_main&origURL=https%3A%2F%2Fgo.gale.com%2Fps%2Fi.do%3Fu%3Dfairfax_main%26v%3D2.1%26it%3Dr%26id%3DGALE%257CK2424100099%26p%3DBIC%26xp%3D&prodId=BIC">"Lawrence Gilman Roberts"</a> World of Computer Science. Gale. 2006. Gale Document Number GALE|K2424100099. Retrieved January 16, 2013. Gale Biography In Context</li>
		<li ><a href="http://news.mit.edu/2003/eecs-time-0521">"Big achievements included room-size computers"</a> MIT News. May 21, 2003. Retrieved January 16, 2013.</li>
		<li><a href="https://www.wsj.com/articles/SB982004616905008338">Draper Prize Honors Four 'Fathers of the Internet. </a></li>
		<li><a href="https://www.nytimes.com/2000/06/04/business/donald-w-davies-75-dies-helped-refine-data-networks.html">"Donald W. Davies, 75, Dies; Helped Refine Data Networks"</a></li>
		<li><a href="http://www.ismlab.usf.edu/dcom/Ch10_Roberts_EvolutionPacketSwitching_IEEE_1978.pdf">Roberts, Dr. Lawrence G. (November 1978). "The Evolution of Packet Switching" (PDF).</a> IEEE Invited Paper. Retrieved September 10, 2017.</li>
	</ul>

		<div class="footp">
		<p>CSC 170 Webpage Design and Development</p>
		<p>Original Source: <a href="http://csc170.org/yzhao75/project1/">yzhao75</a></p>
		<p>@2019 Boqing Zheng</p>
	</div>

	</footer>

<?php include "inc/scripts.php"; ?>

</body>
</html>