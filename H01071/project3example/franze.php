<?php
  include "inc/html-top.php";
?>

<body>

<?php
  include "inc/nav.php";
?>

<main>	

<div class="container">
	<section class="briefintro">
		<h2 class="h21" id="main">Introduction</h2>

			<div class="paraset1">
			<p><strong>Franz Uri Boas</strong> (1858–1942) was a German-born American anthropologist and a pioneer of modern anthropology who has been called the "Father of American Anthropology". His work is associated with the movement of anthropological historicism.</p>
			
			<p>Studying in Germany, Boas was awarded a doctorate in 1881 in physics while also studying geography. He then participated in a geographical expedition to northern Canada, where he became fascinated with the culture and language of the Baffin Island Inuit. He went on to do field work with the indigenous cultures and languages of the Pacific Northwest. In 1887 he emigrated to the United States, where he first worked as a museum curator at the Smithsonian, and in 1899 became a professor of anthropology at Columbia University, where he remained for the rest of his career. </p>
			
			<p>Boas also introduced the ideology of <em>cultural relativism </em>, which holds that cultures cannot be objectively ranked as higher or lower, or better or more correct, but that all humans see the world through the lens of their own culture and judge it according to their own culturally acquired norms. For Boas, the object of anthropology was to understand the way in which culture conditioned people to understand and interact with the world in different ways and to do this it was necessary to gain an understanding of the language and cultural practices of the people studied.  </p>			
			</div>

			<figure class="img1">
				<img src="images/boas.png" alt="Formal Photo of Boas">
				<figcaption>Franz Uri Boas</figcaption>
			</figure>
	</section>

	<div class="section2">
		<h2 class="h22" id="early">Early life and education</h2>

			<p class="p21">
				Through his students, many of whom went on to found anthropology departments and research programmes inspired by their mentor, Boas profoundly influenced the development of American anthropology. Among his most significant students were A. L. Kroeber, Ruth Benedict, Edward Sapir, Margaret Mead, Zora Neale Hurston, and many others. By uniting the disciplines of archaeology, the study of material culture and history, and physical anthropology, the study of variation in human anatomy, with ethnology, the study of cultural variation of customs, and descriptive linguistics, the study of unwritten indigenous languages, Boas created the four-field subdivision of anthropology which became prominent in American anthropology in the 20th century.Franz Boas was born on July 9, 1858, in Minden, Westphalia, the son of Sophie Meyer and Meier Boas. Although his grandparents were observant Jews, his parents embraced Enlightenment values, including their assimilation into modern German society. Boas's parents were educated, well-to-do, and liberal; they did not like dogma of any kind. Due to this, Boas was granted the independence to think for himself and pursue his own interests. Early in life, he displayed a penchant for both nature and natural sciences. Boas vocally opposed antisemitism and refused to convert to Christianity, but he did not identify himself as a Jew.This is disputed however by Ruth Bunzel, a protégée of Boas, who called him "the essential protestant; he valued autonomy above all things."
			</p>
			
			<p class="p22">
				From kindergarten on, Boas was educated in natural history, a subject he enjoyed. In gymnasium, he was most proud of his research on the geographic distribution of plants. When he started his university studies, Boas first attended Heidelberg University for a semester followed by four terms at Bonn University, studying physics, geography, and mathematics at these schools. In 1879, he hoped to transfer to Berlin University to study physics under Hermann von Helmholtz, but ended up transferring to the University of Kiel instead due to family reasons. At Kiel, Boas studied under Theobald Fischer and received a doctorate in physics in 1881 for his dissertation entitled Contributions to the Understanding of the Color of Water, which examined the absorption, reflection, and the polarization of light in seawater. Although technically Boas' doctorate degree was in physics, his advisor Fischer, a student of Carl Ritter, was primarily a geographer and thus some biographers view Boas as more of a geographer than a physicist at this stage. The combination of physics and geography also may have been accomplished through a major in physics and a minor in geography. For his part Boas self-identified as a geographer by this time, prompting his sister, Toni, to write in 1883 "After long years of infidelity, my brother was re-conquered by geography, the first love of his boyhood."
			</p>
	</div>	

</div>




<div class="gridbox1">
		<h2 class="h23" id="career">Early career: museum studies</h2>

		<div class="box1">
			<figure class="img2">
				<img src="images/boas2.png" alt="Franz Boas">
				<figcaption>Boas posing in US National Museum</figcaption>
			</figure>
			<p>
				In his dissertation research, Boas' methodology included investigating how different intensities of light created different colors when interacting with different types of water, however, he encountered difficulty in being able to objectively perceive slight differences in the color of water and as a result became intrigued by this problem of perception and its influence on quantitative measurements. Boas, due to tone deafness, encountered difficulties studying tonal languages such as Laguna. Boas had already been interested in Kantian philosophy since taking a course on aesthetics with Kuno Fischer at Heidelberg. These factors led Boas to consider pursuing research in psychophysics, which explores the relationship between the psychological and the physical, after completing his doctorate, but he had no training in psychology. Boas did publish six articles on psychophysics during his year of military service (1882–1883), but ultimately he decided to focus on geography, primarily so he could receive sponsorship for his planned Baffin Island expedition.
			</p>
		</div>

		<div class="box2">
			<p>
				In the late 19th century anthropology in the United States was dominated by the Bureau of American Ethnology, directed by John Wesley Powell, a geologist who favored Lewis Henry Morgan's theory of cultural evolution. The BAE was housed at the Smithsonian Institution in Washington, and the Smithsonian's curator for ethnology, Otis T. Mason, shared Powell's commitment to cultural evolution. (The Peabody Museum at Harvard University was an important, though lesser, center of anthropological research.)
			</p>

			<p>
				It was while working on museum collections and exhibitions that Boas formulated his basic approach to culture, which led him to break with museums and seek to establish anthropology as an academic discipline.During this period Boas made five more trips to the Pacific Northwest. His continuing field research led him to think of culture as a local context for human action. His emphasis on local context and history led him to oppose the dominant model at the time, cultural evolution.
			</p>

			<p>
				Boas initially broke with evolutionary theory over the issue of kinship. Lewis Henry Morgan had argued that all human societies move from an initial form of matrilineal organization to patrilineal organization. First Nations groups on the northern coast of British Columbia, like the Tsimshian, and Tlingit, were organized into matrilineal clans. First Nations on the southern coast, like the Nootka and the Salish, however, were organized into patrilineal groups. Boas focused on the Kwakiutl, who lived between the two clusters. The Kwakiutl seemed to have a mix of features. 
			</p>
		</div>

		<div class="box3">

			<p>
				Prior to marriage, a man would assume his wife's father's name and crest. His children took on these names and crests as well, although his sons would lose them when they got married. Names and crests thus stayed in the mother's line. At first, Boas—like Morgan before him—suggested that the Kwakiutl had been matrilineal like their neighbors to the north, but that they were beginning to evolve patrilineal groups. In 1897, however, he repudiated himself, and argued that the Kwakiutl were changing from a prior patrilineal organization to a matrilineal one, as they learned about matrilineal principles from their northern neighbors.Boas's rejection of Morgan's theories led him, in an 1887 article, to challenge Mason's principles of museum display. At stake, however, were more basic issues of causality and classification. The evolutionary approach to material culture led museum curators to organize objects on display according to function or level of technological development. Curators assumed that changes in the forms of artifacts reflect some natural process of progressive evolution. Boas, however, felt that the form an artifact took reflected the circumstances under which it was produced and used. Arguing that "[t]hough like causes have like effects like effects have not like causes", Boas realized that even artifacts that were similar in form might have developed in very different contexts, for different reasons. Mason's museum displays, organized along evolutionary lines, mistakenly juxtapose like effects; those organized along contextual lines would reveal like causes.
			</p>
		</div>

</div>



<div class="container2">

	<div class="gridbox2">

		<h2 class="h24" id="contributions">Later career: academic anthropology</h2>
			<div class="grid1">

			<figure>
				<img src="images/columbiauniversity.png" alt="Columbia University">
				<figcaption>Columbia Universitynin 1903</figcaption>
			</figure>

			<p>
				Boas was appointed a lecturer in physical anthropology at Columbia University in 1896, and promoted to professor of anthropology in 1899. However, the various anthropologists teaching at Columbia had been assigned to different departments. When Boas left the Museum of Natural History, he negotiated with Columbia University to consolidate the various professors into one department, of which Boas would take charge. Boas's program at Columbia became the first Doctor of Philosophy (PhD) program in anthropology in America.
			</p>
		</div>


		<div class="grid2">
			<p>
				During this time Boas played a key role in organizing the <em>American Anthropological Association (AAA)</em> as an umbrella organization for the emerging field. Boas originally wanted the AAA to be limited to professional anthropologists, but W. J. McGee (another geologist who had joined the BAE under Powell's leadership) argued that the organization should have an open membership. McGee's position prevailed and he was elected the organization's first president in 1902; Boas was elected a vice-president, along with Putnam, Powell, and Holmes.
			</p>
		</div>

		<div class="grid3">
			<p>
				At both Columbia and the AAA, Boas encouraged the "four-field" concept of anthropology; he personally contributed to physical anthropology, linguistics, archaeology, as well as cultural anthropology. His work in these fields was pioneering: in physical anthropology he led scholars away from static taxonomical classifications of race, to an emphasis on human biology and evolution; in linguistics he broke through the limitations of classic philology and established some of the central problems in modern linguistics and cognitive anthropology; in cultural anthropology he (along with the Polish-English anthropologist Bronislaw Malinowski) established the contextualist approach to culture, cultural relativism, and the participant observation method of fieldwork.
			</p>
		</div>

		<div class="grid4">			
		<p>
			One of Boas's most important books, The Mind of Primitive Man (1911), integrated his theories concerning the history and development of cultures and established a program that would dominate American anthropology for the next fifteen years. In this study, he established that in any given population, biology, language, material, and symbolic culture, are autonomous; that each is an equally important dimension of human nature, but that no one of these dimensions is reducible to another. In other words, he established that culture does not depend on any independent variables. He emphasized that the biological, linguistic, and cultural traits of any group of people are the product of historical developments involving both cultural and non-cultural forces. He established that cultural plurality is a fundamental feature of humankind and that the specific cultural environment structures much individual behavior.
		</p>
		</div>
</div>

	
<div class="writing">
	<h2 class="h27" id="awards">Writings</h2>
		<ul>
			<li>
			<strong>The Houses of the Kwakiutl Indians, British Columbia</strong>(1889)
			</li>
			<li>
			<strong>The Social Organization and the Secret Societies of the Kwakiutl Indians</strong> (1895)	
			</li>
			<li>
			<strong>The Decorative Art of the Indians of the North Pacific Coast</strong> (1897)
			</li>
			<li>
			<strong>The Mythology of the Bella Coola Indians</strong> (1898)
			</li>
			<li>
			<strong>The Mythology of the Bella Coola Indians </strong> (1900)
			</li>
		</ul>
</div>




<h2 class="h26" id="development">Famous Students of Boas</h2>
	<table class="boastable">
		<tr>
			<th>Name</th>
			<th>Year of Birth</th>
			<th>Work</th>
		</tr>

		<tr>
			<th>A. L. Kroeber</th>
			<th>1876</th>
			<th><em>Indian Myths of South Central California</em></th>
		</tr>

		<tr>
			<th>Ruth Benedict</th>
			<th>1887</th>
			<th><em>Patterns of Culture</em></th>
		</tr>

		<tr>
			<th>Edward Sapir</th>
			<th>1884</th>
			<th><em>Yana Texts</em></th>
		</tr>

		<tr>
			<th>Margaret Mead</th>
			<th>1901</th>
			<th><em>Coming of Age in Samoa</em></th>
		</tr>

		<tr>
			<th>Zora Neale Hurston</th>
			<th>1891</th>
			<th><em>Barracoon: The Story of the Last "Black Cargo"</em></th>
		</tr>
	</table>

</div>

</main>

<footer>
	<h2 class="h28">Citations</h2>
		<ul class="citation">
			<li><a href="https://doi.org/10.1177%2F0308275x9401400205">Baker, Lee D. (1994). "The Location of Franz Boas Within the African American Struggle". Critique of Anthropology. 14 (2): 199–217. doi:10.1177/0308275x9401400205</a></li>
			<li><a href="https://journals.sagepub.com/doi/10.1177/1463499604040846">Baker, Lee D. (2004). "Franz Boas Out of the Ivory Tower". Anthropological Theory. 4 (1): 29–51. doi:10.1177/1463499604040846</a></li>
		</ul>

	<div class="footp">
		<p>CSC 170 Webpage Design and Development</p>
		<p>Original Source: <a href="http://www.csc170.org/rpeng2/project1/">rpeng2</a></p>
		<p>@2019 Boqing Zheng</p>
	</div>
			
</footer>	

<?php include "inc/scripts.php"; ?>

</body>

</html>