<!DOCTYPE html>
<html lang="en-US">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Alan Turing</title>
  <link rel="stylesheet"  href="css/home.css">
  <link rel="stylesheet"  href="css/styles.css">
  <style type="text/css">
    #alan {
      background-color: black;
      color: yellow;
    }
  </style>
</head>

<body>
  <?php include "inc/header.php" ?>

  <!-- this is the introduction -->
  
  <div class="container">
	<header class = "header head">  
		<h1 class="title">Alan Turing</h1>  
		<div class = "intro"><em>The father of theoretical computer science.</em></div>
	</header>

	<div class="introduction">
	<section>
                <h2>Overview</h2>
                <article>
                    <img src="images/alan1.jpeg" alt="Alan Turing">
                    <p>
                        <strong>Alan Mathison Turing</strong> <em>(23 June 1912 – 7 June 1954)</em> was an English mathematician, computer scientist, logician, cryptanalyst, philosopher, and theoretical biologist.Turing was highly influential in the development of theoretical computer science, providing a formalisation of the concepts of algorithm and computation with the Turing machine, which can be considered a model of a general-purpose computer. Turing is widely considered to be the father of theoretical computer science and artificial intelligence.Despite these accomplishments, he was never fully recognised in his home country during his lifetime due to the prevalence of homophobia at the time and because much of his work was covered by the Official Secrets Act.
                    </p>
                    <p>
                        During the Second World War, Turing worked for the Government Code and Cypher School (GC&CS) at Bletchley Park, Britain's codebreaking centre that produced Ultra intelligence. For a time he led Hut 8, the section that was responsible for German naval cryptanalysis. Here, he devised a number of techniques for speeding the breaking of German ciphers, including improvements to the pre-war Polish bombe method, an electromechanical machine that could find settings for the Enigma machine.
                    </p>
                    <p>
                        Turing played a crucial role in cracking intercepted coded messages that enabled the Allies to defeat the Nazis in many crucial engagements, including the Battle of the Atlantic, and in so doing helped win the war. Due to the problems of counterfactual history, it is hard to estimate the precise effect Ultra intelligence had on the war, but at the upper end it has been estimated that this work shortened the war in Europe by more than two years and saved over 14 million lives.
                    </p>
                    <p>
                        After the war Turing worked at the National Physical Laboratory, where he designed the Automatic Computing Engine. The Automatic Computing Engine was one of the first designs for a stored-program computer. In 1948, Turing joined Max Newman's Computing Machine Laboratory, at the Victoria University of Manchester, where he helped develop the Manchester computers and became interested in mathematical biology. He wrote a paper on the chemical basis of morphogenesis and predicted oscillating chemical reactions such as the Belousov–Zhabotinsky reaction, first observed in the 1960s.
                    </p>
                    <p>
                        Turing was prosecuted in 1952 for homosexual acts; the Labouchere Amendment of 1885 had mandated that "gross indecency" was a criminal offence in the UK. He accepted chemical castration treatment, with DES, as an alternative to prison. Turing died in 1954, 16 days before his 42nd birthday, from cyanide poisoning. An inquest determined his death as a suicide, but it has been noted that the known evidence is also consistent with accidental poisoning.
                    </p>
                    <p>
                        In 2009, following an Internet campaign, British Prime Minister Gordon Brown made an official public apology on behalf of the British government for "the appalling way he was treated". Queen Elizabeth II granted Turing a posthumous pardon in 2013. The "Alan Turing law" is now an informal term for a 2017 law in the United Kingdom that retroactively pardoned men cautioned or convicted under historical legislation that outlawed homosexual acts.
                    </p>
                </article>
            </section>


            <section>
                <h2><em>Early life and education</em></h2>
            
                <article>
                    <h3>Family</h3>
                    <p><strong>Turing</strong> was born in Maida Vale, London, while his father, Julius Mathison Turing (1873–1947), was on leave from his position with the Indian Civil Service (ICS) at Chatrapur, then in the Madras Presidency and presently in Odisha state, in India. Turing's father was the son of a clergyman, the Rev. John Robert Turing, from a Scottish family of merchants that had been based in the Netherlands and included a baronet. Turing's mother, Julius's wife, was Ethel Sara Turing ( Stoney 1881–1976), daughter of Edward Waller Stoney, chief engineer of the . The Stoneys were a Protestant Anglo-Irish gentry family from both County Tipperary and County Longford, while Ethel herself had spent much of her childhood in County Clare.
                    </p> 
                    <p>
                        Julius's work with the ICS brought the family to British India, where his grandfather had been a general in the Bengal Army. However, both Julius and Ethel wanted their children to be brought up in Britain, so they moved to Maida Vale, London, where Alan Turing was born on 23 June 1912, as recorded by a blue plaque on the outside of the house of his birth, later the Colonnade Hotel.Turing had an elder brother, John (the father of Sir John Dermot Turing, 12th Baronet of the Turing baronets).
                    </p>
                    <p>Turing's father's civil service commission was still active and during Turing's childhood years Turing's parents travelled between Hastings in the United Kingdom and India, leaving their two sons to stay with a retired Army couple. At Hastings, Turing stayed at Baston Lodge, Upper Maze Hill, St Leonards-on-Sea, now marked with a blue plaque. The plaque was unveiled on 23 June 2012, the centenary of Turing's birth.Very early in life, Turing showed signs of the genius that he was later to display prominently. His parents purchased a house in Guildford in 1927, and Turing lived there during school holidays. The location is also marked with a blue plaque.                     
                    </p>
                </article>


                    <h3>Education History</h3>
                    <table border="1">
                         <tr>
                             <th>Time</th>
                             <th>School</th>
                         </tr>
                         <tr>
                             <td>1918-1922</td>
                             <td>St Michael’s day school</td>
                         </tr>
                         <tr>
                             <td>1922-1926</td>
                             <td>Hazelhurst Preparatory School</td>
                         </tr>
                         <tr>
                             <td>1926-1931</td>
                             <td>Sherborne school</td>
                         </tr>
                         <tr>
                             <td>1931-1934</td>
                             <td>King’s College</td>
                         </tr>
                         <tr>
                             <td>1936-1938</td>
                             <td>Princeton University</td>
                         </tr>
                    </table>
                
                

                <article>
                    <h3>University and work on computability</h3>
                    <p>
                        After Sherborne, Turing studied as an undergraduate from 1931 to 1934 at King's College, Cambridge, where he was awarded first-class honours in mathematics. In 1935, at the age of 22, he was elected a Fellow of King's College on the strength of a dissertation in which he proved the central limit theorem. Unknown to the committee, the theorem had already been proven, in 1922, by Jarl Waldemar Lindeberg. A blue plaque at the college was unveiled on the centenary of his birth on 23 June 2012 and is now installed at the college's Keynes Building on King's Parade.
                    </p>
                    <p>
                        In 1936, Turing published his paper "On Computable Numbers, with an Application to the Entscheidungsproblem". It was published in the Proceedings of the London Mathematical Society journal in two parts, the first on 30 November and the second on 23 December. In this paper, Turing reformulated Kurt Gödel's 1931 results on the limits of proof and computation, replacing Gödel's universal arithmetic-based formal language with the formal and simple hypothetical devices that became known as Turing machines. The Entscheidungsproblem (decision problem) was originally posed by German mathematician David Hilbert in 1928. Turing proved that his "universal computing machine" would be capable of performing any conceivable mathematical computation if it were representable as an algorithm. He went on to prove that there was no solution to the decision problem by first showing that the halting problem for Turing machines is undecidable: it is not possible to decide algorithmically whether a Turing machine will ever halt. This paper has been called "easily the most influential math paper in history".
                    </p>

                    <figure>
                        <img src="images/alan2.jpeg" alt="Turing machine">
                        <figcaption><em>Picture of a Turing Machine</em></figcaption>
                    </figure>

                    <p>
                        Although Turing's proof was published shortly after Alonzo Church's equivalent proof using his lambda calculus,[50] Turing's approach is considerably more accessible and intuitive than Church's. It also included a notion of a 'Universal Machine' (now known as a universal Turing machine), with the idea that such a machine could perform the tasks of any other computation machine (as indeed could Church's lambda calculus). According to the Church–Turing thesis, Turing machines and the lambda calculus are capable of computing anything that is computable. John von Neumann acknowledged that the central concept of the modern computer was due to Turing's paper. To this day, Turing machines are a central object of study in theory of computation.
                    </p>
                    <p>
                        From September 1936 to July 1938, Turing spent most of his time studying under Church at Princeton University. in the second year as a Jane Eliza Procter Visiting Fellow. In addition to his purely mathematical work, he studied cryptology and also built three of four stages of an electro-mechanical binary multiplier. In June 1938, he obtained his PhD from the Department of Mathematics at Princeton; his dissertation, Systems of Logic Based on Ordinals, introduced the concept of ordinal logic and the notion of relative computing, in which Turing machines are augmented with so-called oracles, allowing the study of problems that cannot be solved by Turing machines. John von Neumann wanted to hire him as his postdoctoral assistant, but he went back to the United Kingdom.
                    </p>
                        
                </article>
            </section>

            <section>
                <h2><em>Career and research</em></h2>

                <aside>
                    <h3>main accomplishments</h3>
                    <ul>
                        <li>invented Turing Machine</li>
                        <li>Invented Bombe</li>
                        <li>Cryptanalysis on Naval Enigma</li>
                        <li>Invented Turingery</li>
                        <li>Invented Delilah</li>
                        <li>Invented Turing Test</li>
                    </ul>
                </aside>

                <article>
                    <h3>Cryptanalysis</h3>
                    <p>
                        During the Second World War, Turing was a leading participant in the breaking of German ciphers at Bletchley Park. The historian and wartime codebreaker Asa Briggs has said, "You needed exceptional talent, you needed genius at Bletchley and Turing's was that genius."
                    </p>
                    <p>
                        From September 1938, Turing worked part-time with the Government Code and Cypher School (GC&CS), the British codebreaking organisation. He concentrated on cryptanalysis of the Enigma cipher machine used by Nazi Germany, together with Dilly Knox, a senior GC&CS codebreaker. Soon after the July 1939 meeting near Warsaw at which the Polish Cipher Bureau gave the British and French details of the wiring of Enigma machine's rotors and their method of decrypting Enigma machine's messages, Turing and Knox developed a broader solution. The Polish method relied on an insecure indicator procedure that the Germans were likely to change, which they in fact did in May 1940. Turing's approach was more general, using crib-based decryption for which he produced the functional specification of the bombe (an improvement on the Polish Bomba).
                    </p>
                    <p>
                        On 4 September 1939, the day after the UK declared war on Germany, Turing reported to Bletchley Park, the wartime station of GC&CS. Specifying the bombe was the first of five major cryptanalytical advances that Turing made during the war. The others were: deducing the indicator procedure used by the German navy; developing a statistical procedure dubbed Banburismus for making much more efficient use of the bombes; developing a procedure dubbed Turingery for working out the cam settings of the wheels of the Lorenz SZ 40/42 (Tunny) cipher machine and, towards the end of the war, the development of a portable secure voice scrambler at Hanslope Park that was codenamed Delilah.
                    </p>
                    <p>
                        By using statistical techniques to optimise the trial of different possibilities in the code breaking process, Turing made an innovative contribution to the subject. He wrote two papers discussing mathematical approaches, titled The Applications of Probability to Cryptography and Paper on Statistics of Repetitions, which were of such value to GC&CS and its successor GCHQ that they were not released to the UK National Archives until April 2012, shortly before the centenary of his birth. A GCHQ mathematician, "who identified himself only as Richard," said at the time that the fact that the contents had been restricted for some 70 years demonstrated their importance, and their relevance to post-war cryptanalysis.
                    </p>
                </article>

                <article>
                    <h3>Bombe</h3>
                    <p>
                        Within weeks of arriving at Bletchley Park, Turing had specified an electromechanical machine called the bombe, which could break Enigma more effectively than the Polish bomba kryptologiczna, from which its name was derived. The bombe, with an enhancement suggested by mathematician Gordon Welchman, became one of the primary tools, and the major automated one, used to attack Enigma-enciphered messages.
                    </p>
                    <p>
                        The bombe searched for possible correct settings used for an Enigma message (i.e., rotor order, rotor settings and plugboard settings) using a suitable crib: a fragment of probable plaintext. For each possible setting of the rotors (which had on the order of 1019 states, or 1022 states for the four-rotor U-boat variant), the bombe performed a chain of logical deductions based on the crib, implemented electromechanically.
                    </p>
                    <p>
                        The bombe detected when a contradiction had occurred and ruled out that setting, moving on to the next. Most of the possible settings would cause contradictions and be discarded, leaving only a few to be investigated in detail. A contradiction would occur when an enciphered letter would be turned back into the same plaintext letter, which was impossible with the Enigma. The first bombe was installed on 18 March 1940.
                    </p>
                    <figure>
                        <img src="images/alan3.jpeg" alt="bombe">
                        <figcaption>Picture of a replica of a Bombe</figcaption>
                    </figure>
                </article>

                <article>
                    <h3>Hut 8 and the naval Enigma</h3>
                    <p>
                        Turing decided to tackle the particularly difficult problem of German naval Enigma "because no one else was doing anything about it and I could have it to myself". In December 1939, Turing solved the essential part of the naval indicator system, which was more complex than the indicator systems used by the other services.
                    </p>
                    <p>
                        That same night, he also conceived of the idea of Banburismus, a sequential statistical technique (what Abraham Wald later called sequential analysis) to assist in breaking the naval Enigma, "though I was not sure that it would work in practice, and was not, in fact, sure until some days had actually broken." For this, he invented a measure of weight of evidence that he called the ban. Banburismus could rule out certain sequences of the Enigma rotors, substantially reducing the time needed to test settings on the bombes. Later this sequential process of accumulating sufficient weight of evidence using decibans (one tenth of a ban) was used in Cryptanalysis of the Lorenz cipher.
                    </p>
                    <p>
                        Turing travelled to the United States in November 1942 and worked with US Navy cryptanalysts on the naval Enigma and bombe construction in Washington; he also visited their Computing Machine Laboratory in Dayton, Ohio.
                    </p>
                </article>

                <article>
                    <h3>Turingery</h3>
                    <p>
                        In July 1942, Turing devised a technique termed Turingery (or jokingly Turingismus) for use against the Lorenz cipher messages produced by the Germans' new Geheimschreiber (secret writer) machine. This was a teleprinter rotor cipher attachment codenamed Tunny at Bletchley Park. Turingery was a method of wheel-breaking, i.e., a procedure for working out the cam settings of Tunny's wheels. He also introduced the Tunny team to Tommy Flowers who, under the guidance of Max Newman, went on to build the Colossus computer, the world's first programmable digital electronic computer, which replaced a simpler prior machine (the Heath Robinson), and whose superior speed allowed the statistical decryption techniques to be applied usefully to the messages. Some have mistakenly said that Turing was a key figure in the design of the Colossus computer. Turingery and the statistical approach of Banburismus undoubtedly fed into the thinking about cryptanalysis of the Lorenz cipher, but he was not directly involved in the Colossus development.
                    </p>
                </article>
                
                <article>
                    <h3>Early computers and the Turing test</h3>
                    <p>
                        Between 1945 and 1947, Turing lived in Hampton, London, while he worked on the design of the ACE (Automatic Computing Engine) at the National Physical Laboratory (NPL). He presented a paper on 19 February 1946, which was the first detailed design of a stored-program computer. Von Neumann's incomplete First Draft of a Report on the EDVAC had predated Turing's paper, but it was much less detailed and, according to John R. Womersley, Superintendent of the NPL Mathematics Division, it "contains a number of ideas which are Dr. Turing's own". Although ACE was a feasible design, the secrecy surrounding the wartime work at Bletchley Park led to delays in starting the project and he became disillusioned. In late 1947 he returned to Cambridge for a sabbatical year during which he produced a seminal work on Intelligent Machinery that was not published in his lifetime. While he was at Cambridge, the Pilot ACE was being built in his absence. It executed its first program on 10 May 1950, and a number of later computers around the world owe much to it, including the English Electric DEUCE and the American Bendix G-15. The full version of Turing's ACE was not built until after his death.
                    </p>
                    <p>
                        According to the memoirs of the German computer pioneer Heinz Billing from the Max Planck Institute for Physics, published by Genscher, Düsseldorf, there was a meeting between Turing and Konrad Zuse. It took place in Göttingen in 1947. The interrogation had the form of a colloquium. Participants were Womersley, Turing, Porter from England and a few German researchers like Zuse, Walther, and Billing (for more details see Herbert Bruderer, Konrad Zuse und die Schweiz).
                    </p>
                    <p>
                        In 1948, Turing was appointed reader in the Mathematics Department at the Victoria University of Manchester. A year later, he became Deputy Director of the Computing Machine Laboratory, where he worked on software for one of the earliest stored-program computers—the Manchester Mark 1. Turing wrote the first version of the Programmer's Manual for this machine, and was recruited by Ferranti as a consultant in the development of their commercialised machine, the Ferranti Mark 1. He continued to be paid consultancy fees by Ferranti until his death. During this time, he continued to do more abstract work in mathematics, and in "Computing Machinery and Intelligence" (Mind, October 1950), Turing addressed the problem of artificial intelligence, and proposed an experiment that became known as the Turing test, an attempt to define a standard for a machine to be called "intelligent". The idea was that a computer could be said to "think" if a human interrogator could not tell it apart, through conversation, from a human being. In the paper, Turing suggested that rather than building a program to simulate the adult mind, it would be better to produce a simpler one to simulate a child's mind and then to subject it to a course of education. A reversed form of the Turing test is widely used on the Internet; the CAPTCHA test is intended to determine whether the user is a human or a computer.
                    </p>
                </article>
            </section>
        </main>

		<footer>
		<h2>References</h2>
            <ul>
                <li>
                    Turing, Alan (1938). Systems of Logic Based on Ordinals (PhD thesis). Princeton University. <a target="_blank" href="https://doi.org/10.1112%2Fplms%2Fs2-45.1.161">doi:10.1112/plms/s2-45.1.161. hdl:21.11116/0000-0001-91CE-3. ProQuest 301792588.</a>
                </li>
                <li>
                    Turing, A.M. (1938). <a target="_blank" href="https://web.archive.org/web/20121023103503/https://webspace.princeton.edu/users/jedwards/Turing%20Centennial%202012/Mudd%20Archive%20files/12285_AC100_Turing_1938.pdf">"Systems of Logic Based on Ordinals" (PDF).</a> Archived from the original (PDF) on 23 October 2012. Retrieved 4 February 2012.
                </li>
                <li>
                    Cora Diamond (ed.), Wittgenstein's Lectures on the Foundations of Mathematics, University of Chicago Press, 1976
                </li>
            </ul>

            <h2>source</h2>
            <div><a target="_blank" href="https://en.wikipedia.org/wiki/Alan_Turing">Alan Turing</a> </div>

				<div class="copyr">
					<div>CSC 170 Webpage Design and Development</div>
					<div>Original Source: <a href="http://www.csc170.org/tliang6/project1/">tliang6</a></div>
					<div>@2020 YOUR_NAME</div>
				</div>

		</footer>
	</div>
</div>

  <?php include "inc/scripts.php" ?>
</body>
</html>