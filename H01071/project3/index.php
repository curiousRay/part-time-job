<!DOCTYPE html>
<html lanh="en-US">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>yourname, CSC 170 Project 3</title>
  <link rel="stylesheet"  href="css/home.css">
  <link rel="stylesheet"  href="css/styles.css">
  <link rel="stylesheet"  href="sss/sss.css">
  <style type="text/css">
    #index {
      background-color: black;
      color: yellow;
    }
  </style>
</head>

<body>
  <?php include "inc/header.php" ?>

  <div class="container" style="width: 20%;">
  <section class="introduction">
    <div class="slider">
      <img class="flexible" src="images/steve1.jpg" alt="Steve Jobs">
      <img class="flexible" src="images/jeff1.jpg" alt="Jeff Bezos" >
      <img class="flexible" src="images/alan1.jpeg" alt="Alan Turing">
      <img class="flexible" src="images/robert1.jpg" alt="Robert Taylor">
    </div>
  </section>
  </div>

  <div class="main-container">

  <section>
  <figure class="el">
			<img src="images/steve1.jpg" alt="Steve_Jobs">
    </figure>
    <div class="child">
      <h2>Steve Jobs</h2>
			<p>
			 	Steven Paul Jobs was born on February 24, 1955, to Abdulfattah Jandali and Joanne Schieble, and was adopted by Paul and Clara Jobs (née Hagopian).
			</p>
    </div>
  </section>


  <section>
  <figure class="el">
			<img src="images/jeff1.jpg" alt="Jeff_Bezos">
    </figure>
    <div class="child">
      <h2>Jeff Bezos</h2>
			<p>
        Jeffrey Preston Bezos is an American internet entrepreneur, industrialist, media proprietor, and investor. He is best known as the founder, CEO, and president of the multi-national technology company Amazon.
			</p>
    </div>
  </section>

  <section>
  <figure class="el">
			<img src="images/alan1.jpeg" alt="Alan_Turing">
    </figure>
    <div class="child">
      <h2>Alan Turing</h2>
      <p>
        Alan Mathison Turing was an English mathematician, computer scientist, logician, cryptanalyst, philosopher, and theoretical biologist. Turing was highly influential in the development of theoretical computer science, providing a formalisation of the concepts of algorithm and computation with the Turing machine.
			</p>
    </div>
  </section>

  <section>
  <figure class="el">
			<img src="images/robert1.jpg" alt="Robert_Taylor">
    </figure>
    <div class="child">
    <h2>Robert Taylor</h2>
			<p>
        Robert William Taylor, known as Bob Taylor, was an American Internet pioneer, who led teams that made major contributions to the personal computer, and other related technologies.
			</p>
    </div>
  </section>
  

	<div class="ending">
		<footer>
				<div class="copyr">
					<div>CSC 170 Webpage Design and Development</div>
					<div>@2020 YOUR_NAME</div>
				</div>
		</footer>
  </div>
  
  
  </div>

  
  <?php include "inc/scripts.php" ?>
</body>
</html>