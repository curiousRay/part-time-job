<header>
  <div class="container" style="grid-row-gap: 0;background-image: linear-gradient( 135deg, yellow 10%, #F55555 100%);
">
    <div class="banner head">
      <img class="logo" src="images/logo.png">
      <section>
        <a href="index.php"><h1 class="site-title">Pioneers of Internet</h1></a>
        <div class="phrase">CSC 170 - Web Design and Development</div>
      </section>
    </div>

  <nav class="banner menu introduction">
    <section>
      <ul>   
        <li><a id="index" href="index.php">Home</a></li>
        <li><a id="steve" href="steve.php">Steve-Jobs</a></li>
        <li><a id="jeff" href="jeff.php">Jeff-Bezos</a></li>
        <li><a id="alan" href="alan.php">Alan-Turing</a></li>
        <li><a id="robert" href="robert.php">Robert Taylor</a></li>
      </ul>
    </section>
  </nav>

  </div>

</header>


