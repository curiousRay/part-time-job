<!DOCTYPE html>
<html lang="en-US">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Robert Taylor</title>
  <link rel="stylesheet"  href="css/home.css">
	<link rel="stylesheet"  href="css/styles.css">
	<style type="text/css">
    #robert {
      background-color: black;
      color: yellow;
    }
  </style>
</head>

<body>
  <?php include "inc/header.php" ?>

  <!-- this is the introduction -->
  
  <div class="container">
	<header class = "header head">  
		<h1 class="title">Robert Taylor</h1>  
		<div class = "intro"><em>An American Internet pioneer.</em></div>
	</header>

	<div class="introduction">
	<figure>
			<img src="images/robert1.jpg" alt="Bob Taylor in 2008">
			<figcaption>Bob Taylor in 2008</figcaption>
		</figure>

		<p>Robert William Taylor (February 10, 1932 – April 13, 2017), known as Bob Taylor, was an American Internet pioneer, who led teams that made major contributions to the personal computer, and other related technologies. He was director of ARPA's Information Processing Techniques Office from 1965 through 1969, founder and later manager of Xerox PARC's Computer Science Laboratory from 1970 through 1983, and founder and manager of Digital Equipment Corporation's Systems Research Center until 1996.</p>

		<p>Uniquely, Taylor had no formal academic training or research experience in computer science; Severo Ornstein likened Taylor to a "concert pianist without fingers," a perception reaffirmed by historian Leslie Berlin: "Taylor could hear a faint melody in the distance, but he could not play it himself. He knew whether to move up or down the scale to approximate the sound, he could recognize when a note was wrong, but he needed someone else to make the music."</p>

		<p>His awards include the National Medal of Technology and Innovation and the Draper Prize. Taylor was known for his high-level vision: "The Internet is not about technology; it's about communication. The Internet connects people who have shared interests, ideas and needs, regardless of geography."</p>

		<section>

			<h2>Early Life</h2>

			<p>Robert W. Taylor was born in Dallas, Texas, in 1932. His adoptive father, Rev. Raymond Taylor, was a Methodist minister who held degrees from Southern Methodist University, the University of Texas at Austin and Yale Divinity School. The family (including Taylor's adoptive mother, Audrey) was highly itinerant during Taylor's childhood, moving from parish to parish. Having skipped several grades as a result of his enrollment in an experimental school, he began his higher education at Southern Methodist University at the age of 16 in 1948; while there, he was "not a serious student" but "had a good time."</p>

			<p>Taylor then served a stint in the United States Naval Reserve during the Korean War (1952–1954) at Naval Air Station Dallas before returning to his studies at the University of Texas at Austin under the GI Bill. At UT he was a "professional student," taking courses for pleasure. In 1957, he earned an undergraduate degree in experimental psychology from the institution with minors in mathematics, philosophy, English and religion.</p>

			<p>He subsequently earned a master's degree in psychology from Texas in 1959 before electing not to pursue a PhD in the field. Reflecting his background in experimental psychology and mathematics, he completed research in neuroscience, psychoacoustics and the auditory nervous system as a graduate student. According to Taylor, "I had a teaching assistantship in the department, and they were urging me to get a PhD, but to get a PhD in psychology in those days, maybe still today, you have to qualify and take courses in abnormal psychology, social psychology, clinical psychology, child psychology, none of which I was interested in. Those are all sort of in the softer regions of psychology. They're not very scientific, they're not very rigorous. I was interested in physiological psychology, in psychoacoustics or the portion of psychology which deals with science, the nervous system, things that are more like applied physics and biology, really, than they are what normally people think of when they think of psychology. So I didn't want to waste time taking courses in those other areas and so I said I'm not going to get a PhD."</p>

			<p>Taylor took engineering jobs with aircraft companies at better salaries. He helped to design the MGM-31 Pershing as a senior systems engineer for defense contractor Martin Marietta (1960–1961) in Orlando, Florida. In 1962, he was invited to join NASA's Office of Advanced Research and Technology as a program manager assigned to the manned flight control and display division after submitting a research proposal for a flight control simulation display.</p>

		</section>

		<section>

			<h2>Computer Career</h2>

			<p>Taylor worked for NASA in Washington, D.C. while the Kennedy administration was backing research and development projects such as the Apollo program for a manned moon landing. In late 1962 Taylor met J. C. R. Licklider, who was heading the new Information Processing Techniques Office (IPTO) of the Advanced Research Project Agency (ARPA) of the United States Department of Defense. Like Taylor, Licklider had specialized in psychoacoustics during his graduate studies. In March 1960, he published "Man-Computer Symbiosis," an article that envisioned new ways to use computers. This work was an influential roadmap in the history of the internet and the personal computer, and greatly influenced Taylor.</p>

			<p>During this period, Taylor also became acquainted with Douglas Engelbart at the Stanford Research Institute in Menlo Park, California. He directed NASA funding to Engelbart's studies of computer-display technology at SRI that led to the computer mouse. The public demonstration of a mouse-based user interface was later called "the Mother of All Demos." At the Fall 1968 Joint Computer Conference in San Francisco, Engelbart, Bill English, Jeff Rulifson and the rest of the Human Augmentation Research Center team at SRI showed on a big screen how he could manipulate a computer remotely located in Menlo Park, while sitting on a San Francisco stage, using his mouse.[10]</p>
			<aside>
				<h3>ARPA</h3>
				<p>In 1965, Taylor moved from NASA to IPTO, first as a deputy to Ivan Sutherland (who returned to academia shortly thereafter) to fund large programs in advanced research in computing at major universities and corporate research centers throughout the United States. Among the computer projects that ARPA supported was time-sharing, in which many users could work at terminals to share a single large computer. Users could work interactively instead of using punched cards or punched tape in a batch processing style. Taylor's office in the Pentagon had a terminal connected to time-sharing at Massachusetts Institute of Technology, a terminal connected to the Berkeley Timesharing System at the University of California, Berkeley, and a third terminal to the System Development Corporation in Santa Monica, California. He noticed each system developed a community of users, but was isolated from the other communities.</p>
				<p>Taylor hoped to build a computer network to connect the ARPA-sponsored projects together, if nothing else, to let him communicate to all of them through one terminal. By June 1966, Taylor had been named director of IPTO; in this capacity, he shepherded the ARPANET project until 1969. Taylor had convinced ARPA director Charles M. Herzfeld to fund a network project earlier in February 1966, and Herzfeld transferred a million dollars from a ballistic missile defense program to Taylor's budget. Taylor hired Larry Roberts from MIT Lincoln Laboratory to be its first program manager. Roberts first resisted moving to Washington DC, until Herzfeld reminded the director of Lincoln Laboratory that ARPA dominated its funding. Licklider continued to provide guidance, and Wesley A. Clark suggested the use of a dedicated computer, called the Interface Message Processor at each node of the network instead of centralized control. At the 1967 Symposium on Operating Systems Principles, a member of Donald Davies' team (Roger Scantlebury) presented their research on packet switching and suggested it for use in the ARPANET. ARPA issued a request for quotation (RFQ) to build the system, which was awarded to Bolt, Beranek and Newman (BBN). ATT Bell Labs and IBM Research were invited to join, but were not interested. At a pivotal meeting in 1967 most participants resisted testing the new network; they thought it would slow down their research.</p>

				<p>In 1968, Licklider and Taylor published "The Computer as a Communication Device". The article laid out the future of what the Internet would eventually become.[16] It began with a prophetic statement: "In a few years, men will be able to communicate more effectively through a machine than face to face."</p>

				<p>Beginning in 1967, Taylor was sent by ARPA to investigate inconsistent reports coming from the Vietnam War. Only 35 years old, he was given an identification card with the military rank equivalent to his civilian position (brigadier general), thus ensuring protection under the Geneva convention if he were captured. Over the course of several trips to the area, he established a computer center at the Military Assistance Command, Vietnam base in Saigon. In his words: "After that the White House got a single report rather than several. That pleased them; whether the data was any more correct or not, I don't know, but at least it was more consistent."[13] The Vietnam project took him away from directing research, and "by 1969 I knew ARPANET would work. So I wanted to leave."</p>

				<p>The election of Richard Nixon to the presidency and ongoing tensions with Roberts (who, despite maintaining a putatively cordial relationship with Taylor, resented his lack of research experience and appointment to the IPTO directorship) also factored in his decision to leave ARPA. For about a year, he joined Sutherland and David C. Evans at the University of Utah in Salt Lake City, where he had funded a center for research on computer graphics while at ARPA.</p>

				<p>Unable to acclimate to the Church of Jesus Christ of Latter-day Saints-dominated milieu, Taylor moved to Palo Alto, California in 1970 to become associate manager of the Computer Science Laboratory (CSL) at Xerox Corporation's new Palo Alto Research Center.</p>

			</aside>

			<p>Technologies developed at PARC under Taylor's aegis focused on reaching beyond ARPANET to develop what has become the Internet, and the systems that support today's personal computers. They included:</p>

			<ul>
				<li>Powerful personal computers (including the Xerox Alto and later "D-machines") with windowed displays and graphical user interfaces that inspired the Apple Lisa and Macintosh. The Computer Science Laboratory built the Alto, which was conceived by Butler Lampson and designed mostly by Charles P. Thacker, Edward M. McCreight, Bob Sproull and David Boggs. The Learning Research Group of PARC's Systems Science Laboratory (led by Alan Kay) added the software-based "desktop" metaphor.</li>
				<li>Ethernet, which networks local computers within a building or campus; and the first Internet, a network that connected the Ethernet to the ARPANET utilizing PUP (PARC Universal Protocol), forerunner to TCP/IP. It was primarily designed by Robert Metcalfe, Boggs, Thacker and Lampson.</li>
				<li>The electronics and software that led to the laser printer (spearheaded by optical engineer Gary Starkweather, who transferred from Xerox's Webster, New York laboratory to work with CSL) and the Interpress page description language that allowed John Warnock and Chuck Geschke to found Adobe Systems.</li>
				<li>"What-you-see-is-what-you-get" (WYSIWYG) word-processing programs, as exemplified by Bravo, which Charles Simonyi took to Microsoft to serve as the basis for Microsoft Word.</li>
				<li>SuperPaint, a pioneering graphics program and framebuffer computer system developed by Richard Shoup. The software was written in consultation with future Pixar co-founder Alvy Ray Smith, who could not secure an appointment at PARC and was retained as an independent contractor. Although Shoup received a special Emmy Award (shared with Xerox) in 1983 and an Academy Scientific Engineering Award (shared with Smith and Thomas Porter) in 1998 for his achievement, program development continued to be marginalized by PARC, ultimately precipitating Shoup's departure in 1979.</li>
			</ul>

			<aside>
				<figure>
					<img src="images/robert2.jpg" alt="ARPANET map">
					<figcaption>An early map of the ARPANET system</figcaption>
				</figure>
			</aside>

			<p>Belying his lack of programming and engineering experience, Taylor was noted for his strident advocacy of Licklider-inspired distributed personal computing and his ability to maintain collegial and productive relationships between what was widely perceived as the foremost array of the epoch's leading computer scientists. This was exemplified by a weekly staff meeting at PARC (colloquially known as "Dealer" after Edward O. Thorp's Beat the Dealer) in which staff members would lead a discussion about myriad topics. They would sit in a circle of beanbag chairs and open debate was encouraged. According to Kay, the meeting "was part of the larger ARPA community to learn how to argue to illuminate rather than merely to win. ... The main purposes of Dealer -- as invented and implemented by Bob Taylor -- were to deal with how to make things work and make progress without having a formal manager structure. The presentations and argumentation were a small part of a deal session (they did quite bother visiting Xeroids). It was quite rare for anything like a personal attack to happen (because people for the most part came into PARC having been blessed by everyone there -- another Taylor rule -- and already knowing how 'to argue reasonably')."</p>

		</section>

		<section>

			<h2>Retirement and death</h2>
			<p>Taylor retired from DEC in 1996. Following his divorce (coinciding with his departure from Xerox), he lived in a secluded house in Woodside, California.</p>
			<p>On April 13, 2017, he died at his home in Woodside, California. His son said he had suffered from Parkinson's disease and other health problems.</p>
		</section>
		<section>

			<h2>Awards</h2>
			
			<table border="1">
				<tr>
					<th>Year</th>
					<th>Award</th>
					<th>Notes</th>
				</tr>
				<tr>
					<td>1984</td>
					<td>ACM Software Systems Award</td>
					<td>for conceiving and guiding the development of the Xerox Alto System</td>
				</tr>
				<tr>
					<td>1994</td>
					<td>ACM Fellow</td>
					<td>in recognition of the same work</td>
				</tr>
				<tr>
					<td>1999</td>
					<td>National Medal of Technology and Innovation</td>
					<td>for visionary leadership in the development of modern computing technology</td>
				</tr>
				<tr>
					<td>2004</td>
					<td>Draper PRize</td>
					<td>for development of the first practical networked personal computers</td>
				</tr>
				<tr>
					<td>2013</td>
					<td>Computer History Museum Fello</td>
					<td>for leadership in the development of computer networking</td>
				</tr>
			</table>
		</section>
		
		<footer>
		<h2>References</h2>
		<ul>
			<li><a target="_blank" href="https://web.archive.org/web/20130515140833/http://www.computerhistory.org/fellowawards/hall/bios/Robert,Taylor/">"Hall of Fellows – Computer History Museum"</a>. Archived from the original on 2013-05-15.</li>
			<li>Marion Softky (October 11, 2000). <a target="_blank" href="http://www.almanacnews.com/morgue/2000/2000_10_11.taylor.html">"Building the Internet: Bob Taylor won the National Medal of Technology "For visionary leadership in the development of modern computing technology""</a>. The California Almanac. Retrieved March 30, 2011.</li>
			<li> Gary Susswein (September 14, 2009). <a href="https://web.archive.org/web/20100720175751/http://www.utexas.edu/features/2009/09/14/taylor_internet/">"Internet and use of the computer as communication device the 1960s brainchild of psychology alum"</a>. University of Texas Alumni profile. Archived from the original on July 20, 2010. Retrieved March 30, 2011.</li>
		</ul>
		<aside>
			<h2>Source</h2>
			<a href="https://en.wikipedia.org/wiki/Robert_Taylor_(computer_scientist)">Wikipedia: Robert Taylor</a>
		</aside>
				<div class="copyr">
					<div>CSC 170 Webpage Design and Development</div>
					<div>Original Source: <a href="http://www.csc170.org/toneill3/project1/">toneill3</a></div>
					<div>@2020 YOUR_NAME</div>
				</div>

		</footer>
	</div>
</div>
  <?php include "inc/scripts.php" ?>
</body>
</html>