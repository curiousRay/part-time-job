<!DOCTYPE html>
<html lang="en-US">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Jeff Bezos</title>
  <link rel="stylesheet"  href="css/home.css">
	<link rel="stylesheet"  href="css/styles.css">
	<style type="text/css">
    #jeff {
      background-color: black;
      color: yellow;
    }
  </style>
</head>

<body>
  <?php include "inc/header.php" ?>
	
	<!-- this is the introduction -->

  <div class="container">

	<header class="header head">
			<h1 class="title">Jeff Bezos</h1>
				<div class="intro"><em>CEO and Founder of Amazon</em></div>
		</header>

		<section class="introduction"> 

			<h2>About</h2>

			<figure>
				<img src="images/jeff1.jpg" alt="Jeff Bezos">
				<figcaption>Jeff Bezos</figcaption>
			</figure>

			<p> <strong> Jeffrey Preston Bezos </strong> (born January 12, 1964) is an American internet entrepreneur, industrialist, media proprietor, and investor. He is best known as the founder, CEO, and president of the multi-national technology company Amazon. The first centi-billionaire on the Forbes wealth index, Bezos has been the world's richest person since 2017 and was named the <strong>"richest man in modern history"</strong> after his net worth increased to $150 billion in July 2018. According to Forbes, Bezos is the first person in history to have a net worth exceeding $200 billion.</p>

			<p>Born in Albuquerque and raised in Houston and later Miami, Bezos graduated from Princeton University in 1986 with a degree in electrical engineering and computer science. He worked on Wall Street in a variety of related fields from 1986 to early 1994. He founded Amazon in late 1994 on a cross-country road trip from New York City to Seattle. The company began as an online bookstore and has since expanded to a wide variety of other e-commerce products and services, including video and audio streaming, cloud computing, and AI. It is currently the world's largest online sales company, the largest Internet company by revenue, and the world's largest provider of virtual assistants and cloud infrastructure services through its Amazon Web Services branch.</p>

			<p>Bezos founded the aerospace manufacturer and sub-orbital spaceflight services company Blue Origin in 2000. A Blue Origin test flight successfully first reached space in 2015, and the company has upcoming plans to begin commercial suborbital human spaceflight. He also purchased the major American newspaper The Washington Post in 2013 for $250 million and manages many other investments through his Bezos Expeditions venture capital firm.</p>

		</section>

		<section>

			<h2>Business Career</h2>

			<h3>Early Career</h3>

			<p>After Bezos graduated from Princeton University in 1986, he was offered jobs at Intel, Bell Labs, and Andersen Consulting, among others. He first worked at Fitel, a fintech telecommunications start-up, where he was tasked with building a network for international trade. Bezos was promoted to head of development and director of customer service thereafter. He transitioned into the banking industry when he became a product manager at Bankers Trust. He worked there from 1988 to 1990. He then joined D. E. Shaw & Co, a newly founded hedge fund with a strong emphasis on mathematical modelling in 1990 and worked there until 1994. Bezos became D. E. Shaw's fourth senior vice-president at the age of 30.</p>

			<h3>Amazon</h3>

			<figure>
					<img src="images/jeff2.jpg" alt="Amazon"> </figure>
					
			<p>In late 1993, Bezos decided to establish an online bookstore. He left his job at D. E. Shaw and founded Amazon in his garage on July 5, 1994, after writing its business plan on a cross-country drive from New York City to Seattle. Bezos initially named his new company <em>Cadabra</em> but later changed the name to Amazon after the Amazon River in South America, in part because the name begins with the letter A, which is at the beginning of the alphabet. He accepted an estimated $300,000 from his parents and invested in Amazon. He warned many early investors that there was a 70% chance that Amazon would fail or go bankrupt. Although Amazon was originally an online bookstore, Bezos had always planned to expand to other products. In 1998, Bezos diversified into the online sale of music and video, and by the end of the year he had expanded the company's products to include a variety of other consumer goods. In October of that year, Amazon was recognized as the largest online shopping retailer in the world. On February 1, 2018, Amazon reported its highest ever profit with quarterly earnings of $2 billion. Due to the growth of Alibaba in China, Bezos has often expressed interest in expanding Amazon into India. On July 27, 2017, Bezos momentarily became the world's wealthiest person over Microsoft co-founder Bill Gates when his estimated net worth increased to just over $90 billion. His wealth surpassed $100 billion for the first time on November 24, 2017, and he was formally designated the wealthiest person in the world by Forbes on March 6, 2018, with a net worth of $112 billion.</p>

		</section>

		<section>

			<h2>Personal Life</h2>

			<p>In 1992, Bezos was working for D. E. Shaw in Manhattan when he met novelist MacKenzie Tuttle, who was a research associate at the firm; the couple married a year later. In 1994, they moved across the country to Seattle, Washington, where Bezos founded Amazon. Bezos and his now ex-wife MacKenzie are the parents of four children: three sons, and one daughter adopted from China.</p>
			
			<p>In 2016, Bezos played a Starfleet official in the movie Star Trek Beyond and joined the cast and crew at a San Diego Comic-Con screening. He had lobbied Paramount for the role and apropos of Alexa and his personal/professional interest in speech recognition. His one line consisted of a response to an alien in distress: <em>"Speak Normally."</em> In his initial discussion of the project which became Alexa with his technical advisor Greg Hart in 2011, Bezos told him that the goal was to create <em>"the Star Trek computer."</em></p>

		</section>

		<aside>

			<h2>Recognitions</h2> 

			<ul>

				<li>In <u>1999</u>, Bezos received his first major award when Time named him Person of the Year.</li>
				<li>In <u>2008</u>, he was selected by U.S. News & World Report as one of America's best leaders.</li>
				<li>Bezos was awarded an honorary doctorate in science and technology from Carnegie Mellon University in <u>2008</u>.</li>
				<li>In <u>2011</u>, The Economist gave Bezos and Gregg Zehr an Innovation Award for the Amazon Kindle.</li>
				<li>In <u>2012</u>, Bezos was named Businessperson of the Year by Fortune.</li>
				<li>In <u>2014</u>, he was ranked the best-performing CEO in the world by Harvard Business Review.</li>
				<li>He has also figured in Fortune's list of 50 great leaders of the world for three straight years, topping the list in <u>2015</u>.</li>
				<li>In September <u>2016</u>, Bezos received a $250,000 prize for winning the Heinlein Prize for Advances in Space Commercialization, which he donated to the Students for the Exploration and Development of Space.</li>
				<li>In February <u>2018</u>, Bezos was elected to the National Academy of Engineering for "leadership and innovation in space exploration, autonomous systems, and building a commercial pathway for human space flight".</li>
				<li>In March <u>2018</u>, at the Explorers Club annual dinner, he was awarded the Buzz Aldrin Space Exploration Award in recognition of his work with Blue Origin.</li>
				<li>Time magazine named him one of the 100 most influential people in the world in their <u>2018</u> listing.</li>
		
			</ul>

		</aside>

		<section>

			<h2>Wealth</h2>

			<p>Bezos first became a millionaire in 1997 after raising $54 million through Amazon's initial public offering (IPO). He was first included on the Forbes World's Billionaires list in 1999 with a registered net worth of $10.1 billion. In 2014, Bezos entered the top ten when he increased his net worth to a total of $50.3 billion. Bezos rose to be the 5th richest person in the world hours before market close; he gained $7 billion in one hour. After sporadic jumps in Amazon's share price, in July 2017 he briefly unseated Microsoft cofounder Bill Gates as the wealthiest person in the world. Bezos would continue to sporadically surpass Gates throughout the month of October 2017 after Amazon's share price fluctuated. His net worth surpassed $100 billion for the first time on November 24, 2017, after Amazon's share price increased by more than 2.5%.On March 6, 2018, Bezos was officially designated the wealthiest person in the world with a registered net worth of $112 billion. He unseated Bill Gates ($90 billion) who was $6 billion ahead of Warren Buffett ($84 billion), ranked third. He is considered the first registered centi-billionaire (not adjusted for inflation).</p>

			<p>His wealth, in 2017–18 terms, equaled that of 2.7 million Americans. According to Quartz, his net worth of $150 billion in July 2018 was enough to purchase the entire stock markets of Nigeria, Hungary, Egypt, Luxembourg, and Iran. On July 17, 2018 he was designated the "wealthiest person in modern history” by the Bloomberg Billionaires Index, Fortune, MarketWatch, The Wall Street Journal, and Forbes.</p>

			<p>Bezos's wealth could have been equitably divided with his ex-wife; however, she eventually received 25% of Bezos's Amazon shares, valued at approximately $36 billion, making her the third richest woman in the world.</p>

		</section>	

		<aside>

			<h2> Jeff Bezos Wealth In Numbers</h2>

			<table border style="width: 100%">
				<tr>
					<th>Year</th>
					<th>Billions</th>
					<th>Change</th>
				</tr>
				<tr>
					<td>1999</td>
					<td>10.1</td>
					<td>0.0%</td>
				</tr>
				<tr>
					<td>2000</td>
					<td>6.1</td>
					<td>-40.5%</td>
				</tr>
				<tr>
					<td>2001</td>
					<td>2.0</td>
					<td>-66.6%</td>
				</tr>
				<tr>
					<td>2002</td>
					<td>1.5</td>
					<td>-25.0%</td>
				</tr>
				<tr>
					<td>2003</td>
					<td>2.5</td>
					<td>66.6%</td>
				</tr>
				<tr>
					<td>2004</td>
					<td>5.1</td>
					<td>104.0%</td>
				</tr>
				<tr>
					<td>2005</td>
					<td>4.1</td>
					<td>5.8%</td>
				</tr>
				<tr>
					<td>2006</td>
					<td>4.3</td>
					<td>10.4%</td>
				</tr>
				<tr>
					<td>2007</td>
					<td>8.7</td>
					<td>102.3%</td>
				</tr>
				<tr>
					<td>2008</td>
					<td>8.2</td>
					<td>5.7%</td>
				</tr>
				<tr>
					<td>2009</td>
					<td>6.8</td>
					<td>17.7%</td>
				</tr>
				<tr>
					<td>2010</td>
					<td>12.6</td>
					<td>85.2%</td>
				</tr>
				<tr>
					<td>2011</td>
					<td>18.1</td>
					<td>43.2%</td>
				</tr>
				<tr>
					<td>2012</td>
					<td>23.2</td>
					<td>28.2%</td>
				</tr>
				<tr>
					<td>2013</td>
					<td>28.9</td>
					<td>24.5%</td>
				</tr>
				<tr>
					<td>2014</td>
					<td>30.5</td>
					<td>5.5%</td>
				</tr>
				<tr>
					<td>2015</td>
					<td>50.3</td>
					<td>60.9%</td>
				</tr>
				<tr>
					<td>2016</td>
					<td>45.2</td>
					<td>10.1%</td>
				</tr>
				<tr>
					<td>2017</td>
					<td>72.8</td>
					<td>61.6%</td>
				</tr>
				<tr>
					<td>2018</td>
					<td>112.0</td>
					<td>53.8%</td>
				</tr>
			</table>

		</aside>

		
	<footer class="ending">

		<h2>Citations</h2>

		<ul>

			<li><em>"Forbes Profile: Jeff Bezos". Forbes. Real Time Net Worth.</em><a target="_blank" href="https://www.forbes.com/sites/angelauyeung/2020/08/18/jeff-bezos-ends-day-with-an-all-time-high-net-worth-of-1978-billion/?utm_source=FBPAGE&utm_medium=social&utm_content=3603051367&utm_campaign=sprinklrForbes+Asia#7b09f25427fd"> https://www.forbes.com/sites/angelauyeung/2020/08/18/jeff-bezos-ends-day-with-an-all-time-high-net-worth-of-1978-billion/?utm_source=FBPAGE&utm_medium=social&utm_content=3603051367&utm_campaign=sprinklrForbes+Asia#7b09f25427fd" </a> </li>
			<li><em>LaGesse, David (November 19, 2008). "America's Best Leaders: Jeff Bezos, Amazon.com.com CEO". U.S. News & World Report. Retrieved November 25,2008.</em> <a target="_blank" href= "https://www.usnews.com/articles/news/best-leaders/2008/11/19/americas-best-leaders-jeff-bezos-amazoncom-ceo.html"> https://www.usnews.com/articles/news/best-leaders/2008/11/19/americas-best-leaders-jeff-bezos-amazoncom-ceo.html</a> </li>
			<li><em>"The centibillionaire club is expanding". The Irish Times. March 21, 2013. Retrieved September 29, 2020.</em><a target="_blank" href="https://www.usnews.com/articles/news/best-leaders/2008/11/19/americas-best-leaders-jeff-bezos-amazoncom-ceo.html">https://www.usnews.com/articles/news/best-leaders/2008/11/19/americas-best-leaders-jeff-bezos-amazoncom-ceo.html</a></li>

		</ul>

		<p>The content on this webapge is from <a target="_blank" href="https://en.wikipedia.org/wiki/Jeff_Bezos"> Wikipedia Jeff Bezos page.</a></p>	
		<div class="copyr">
		<div>CSC 170 Webpage Design and Development</div>
		<div>Original Source: <a href="http://www.csc170.org/amendez/project1/">amendez</a></div>
		<div>@2020 YOUR_NAME</div>
	</div>
	</footer>

		

  <?php include "inc/scripts.php" ?>
</body>
</html>